-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2022 at 03:06 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kulitulis`
--

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE `tbl_company` (
  `id_company` int(5) NOT NULL,
  `company_name` varchar(50) DEFAULT NULL,
  `company_desc` varchar(255) DEFAULT NULL,
  `company_address` text,
  `company_maps` text,
  `company_phone1` char(15) DEFAULT NULL,
  `company_phone2` char(15) DEFAULT NULL,
  `company_fax` char(15) DEFAULT NULL,
  `company_email` varchar(50) DEFAULT NULL,
  `logo` text,
  `logo_type` char(5) NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_by` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`id_company`, `company_name`, `company_desc`, `company_address`, `company_maps`, `company_phone1`, `company_phone2`, `company_fax`, `company_email`, `logo`, `logo_type`, `update_at`, `update_by`) VALUES
(1, 'KULITULIS', 'Kuli tulis adalah sebuah platform bagi para penulis untuk menuangkan sebuah karya tulisnya', 'Jakarta dev', NULL, '088888888', '08888888', '1112', 'kulitulisdev@kulitulis.com', 'kulitulis20220216220403', '.png', '2022-02-16 22:04:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ebook`
--

CREATE TABLE `tbl_ebook` (
  `id_ebook` int(5) NOT NULL,
  `id_jenis` int(5) NOT NULL,
  `id_kategori` int(5) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `slug_ebook` varchar(100) NOT NULL,
  `harga_normal` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `harga_diskon` int(11) DEFAULT NULL,
  `penulis` varchar(100) NOT NULL,
  `penerbit` varchar(100) NOT NULL,
  `tahun_terbit` varchar(4) NOT NULL,
  `stok` enum('Tersedia','Tidak Tersedia') NOT NULL DEFAULT 'Tersedia',
  `best_seller` enum('0','1') NOT NULL DEFAULT '0',
  `foto` text,
  `foto_type` char(15) DEFAULT NULL,
  `sinopsis` text,
  `link_tokped` text,
  `link_shopee` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` char(15) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_by` char(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_ebook`
--

INSERT INTO `tbl_ebook` (`id_ebook`, `id_jenis`, `id_kategori`, `judul`, `slug_ebook`, `harga_normal`, `diskon`, `harga_diskon`, `penulis`, `penerbit`, `tahun_terbit`, `stok`, `best_seller`, `foto`, `foto_type`, `sinopsis`, `link_tokped`, `link_shopee`, `created_at`, `created_by`, `update_at`, `update_by`) VALUES
(1, 1, 2, 'Buku Implementasi Jaringan Komputer', 'buku-implementasi-jaringan-komputer', 150000, 10, 135000, 'Januar Al Amien  dan Harun Mukhtar', '-', '2020', 'Tersedia', '1', 'buku-implementasi-jaringan-komputer20220219091924', '.jpg', '<p><strong>Buku Implementasi Jaringan Komputer</strong></p>\r\n<p>Buku ini berisi tentang jaringan komputer secara implementasi atau praktik langsung pada perangkat jaringan. Buku ini menyajikan ulasan yang mudah dipahami dan diimplementasikan. Buku ini sangat cocok untuk mahasiswa yang sedang belajar jaringan komputer dan juga sangat cocok untuk karyawan maupun administrator jaringan pada perusahaan ataupun kantor yang sedang membangun server. Sysadmin sangat membutuhkan praktik nyata dari buku ini. Perangkat yang dibutuhkan untuk praktik nyata pada buku ini adalah mikrotik. Mikrotik sangat diperlukan untuk uji coba dan menyelesaikan pemahaman yang terdapat pada buku ini</p>\r\n<p><em>Buku Implementasi Jaringan Komputer ini diterbitkan oleh Penerbit Buku Pendidikan Deepublish.</em></p>\r\n<p>Dapatkan buku-buku berkualitas. Kami berfokus menjual buku-buku kuliah untuk Mahasiswa di seluruh Indonesia, dengan pilihan terlengkap kamu pasti mendapatkan buku yang Anda cari.</p>\r\n<p><strong>Kelebihan kami :</strong></p>\r\n<p>*Buku Baru<br> *Original<br> *Pengiriman Cepat<br> *Stok selalu tersedia<br> *Packing aman & rapi<br> *Garansi 100% jika produk rusak/cacat/tidak sesuai KAMI GANTI atau UANG ANDA KEMBALI</p>', 'http://localhost/git/kulitulis', '', '2022-02-16 23:15:04', '1', '2022-02-19 09:19:24', '1'),
(2, 1, 2, 'Algoritma dan Struktur Data dengan C,C++ dan Java Edisi 8', 'algoritma-dan-struktur-data-dengan-cc-dan-java-edisi-8', 150000, 20, 120000, 'Moh. Sjukani ', 'Mitra Wacana Media', '2013', 'Tersedia', '0', 'algoritma-dan-struktur-data-dengan-cc-dan-java-edisi-820220219091910', '.jpg', '<p>Dalam pembuatan sebuah program tidak cukup hanya memahami maksud instruksi-instruksi atau fungsi-fungsi yang terdapat dalam suatu Bahasa Pemrograman. Lebih dari itu yang harus dikuasai adalah bagaimana merangkai instruksi-instruksi atau fungsi-fungsi tersebut sehingga menjadi suatu program dengan algoritma yang benar dan efisien. Buku ini berusaha menunjukkan bahwa dalam menyusun algoritma yang benar dan efisien yang pada awalnya mungkin dianggap sulit, ternyata didalamnya terdapat teknik-teknik jenius yang mengagumkan dan mengasyikkan sehingga merangsang kita untuk ingin menguasai lebih banyak. Untuk tujuan tersebut, buku ini menyampaikan sebanyak mungkin contoh algoritma atau teknik pemrograman yang dihimpun dari berbagai buku literatur, juga mengumpulkan contoh-contoh soal yang biasa diberikan pada Perguruan Tinggi Komputer. Materi dalam buku ini disusun tahap per tahap sesuai dengan tahap yang diberikan pada mata kuliah Algoritma, Pemrograman atau Struktur Data di Perguruan Tinggi Komputer, semua Jurusan atau Program Studi. Juga dilengkapi dengan soal-soal latihan mandiri yang diharapkan selain dapat membantu mahasiswa dalam mencermati ling\'kup masalah yang perlu dipecahkan sesuai dengan teknik algoritma yang diberikan, juga diharapkan dapat membantu dosen dalam melengkapi bank soal.</p>', 'https://www.mitrawacanamedia.com/kategori-komputer-it/algoritma-dan-struktur-data-dengan-c-dan-java-edisi-8', '', '2022-02-19 09:04:56', '1', '2022-02-19 09:19:11', '1'),
(3, 1, 2, 'Algoritma dan Struktur Data dengan C,C++ dan Java Edisi 9', 'algoritma-dan-struktur-data-dengan-cc-dan-java-edisi-9', 150000, 10, 135000, 'david wijaya', 'Mitra Wacana Media', '2014', 'Tersedia', '1', 'algoritma-dan-struktur-data-dengan-cc-dan-java-edisi-920220219091901', '.jpg', '<p>Dalam pembuatan sebuah program tidak cukup hanya memahami maksud instruksi atau fungsi yang terdapat dalam suatu Bahasa Pemrograman. Lebih dari itu yang harus dikuasai adalah bagaimana merangkai instruksi-instruksi atau fungsi-fungsi tersebut sehingga menjadi suatu program dengan algoritma yang benar dan efisien.   Buku Algoritma: (Algoritma dan Struktur Data 1) dengan C, C++, dan Java Edisi 9 ini berusaha menunjukan bahwa dalam menyusun algoritma yang benar dan efisien yang pada awalnya mungkin dianggap sulit, ternyata di dalamnya terdapat teknik-teknik jenius yang mengagumkan dan mengasikan sehingga merangsang untuk ingin menguasai lebih banyak. Untuk tujuan tersebut, buku ini menyampaikan sebanyak mungkin contoh algoritma atau teknik pemrograman yang dihimpun dari berbagai buku literatur, juga mengumpulkan contoh-contoh soal yang biasa diberikan pada Perguruan Tinggi Komputer.   Materi dalam buku ini disusun tahap per tahap sesuai dengan tahap yang berikan pada mata kuliah Algoritma, Pemrograman atau Struktur Data di Perguruan Tinggi Komputer, semua Jurusan atau Program Studi. Juga dilengkapi dengan soal-soal latihan mandiri yang diharapkan selain dapat membantu mahasiswa dalam mencermati lingkup masalah yang perlu dipecahkan sesuai dengan teknik algoritma yang diberikan, juga diharapkan dapat membantu dosen dalam melengkapi bank soal.</p>', 'https://www.mitrawacanamedia.com/kategori-komputer-it/algoritma-dan-struktur-data-dengan-c-dan-java-edisi-9', '', '2022-02-19 09:06:54', '1', '2022-02-19 09:19:01', '1'),
(4, 1, 2, 'Aplikasi Excel dalam penganggaran bisnis', 'aplikasi-excel-dalam-penganggaran-bisnis', 80000, 20, 64000, 'david wijaya', 'Mitra Wacana Media', '2016', 'Tersedia', '0', 'aplikasi-excel-dalam-penganggaran-bisnis20220219091853', '.jpg', '<p>Anggaran merupakan gambaran kuantitatif tentang operasi bisnis di masa lalu, saat ini, dan masa yang akan datang. Oleh sebab itu, perusahaan dari berbagai jenis dan ukuran telah menggunakan anggaran untuk menganalisis serta merencanakan aktivitas bisnis sehari-hari. Anggaran digunakan sebagai dasar penyusunan proyeksi laporan keuangan seperti proyeksi laporan posisi keuangan dan laporan laba rugi komprehensif. Buku ini merupakan buku panduan praktis yang dapat memandu Saudara menyusun anggaran berbasis Microsoft Excel. Setelah selesai membaca buku ini, Saudara dapat memahami tahapan penyusunan anggaran dan aplikasi Microsoft Excel dalam penganggaran bisnis.</p>', 'https://www.mitrawacanamedia.com/kategori-komputer-it/aplikasi-excel-dalam-pengaggaran-bisnis', '', '2022-02-19 09:08:39', '1', '2022-02-19 09:18:53', '1'),
(5, 1, 2, 'Aplikasi Komputer', 'aplikasi-komputer', 120000, 5, 114000, 'Nurhafifah Matondang, Eri Mardiani, dkk', 'Mitra Wacana Media', '2019', 'Tersedia', '1', 'aplikasi-komputer20220219091844', '.jpg', '<p>Aplikasi komputer adalah jenis program komputer yang dibuat sesuai dengan bahasa pemrograman dan digunakan untuk berbagai kebutuhan yang diperlukan. Dalam kehidupan sehari-hari Anda bisa melihat beberapa penggunaan dari aplikasi komputer tersebut untuk bisnis, pendidikan maupun untuk hiburan. Dengan menggunakan beberapa aplikasi komputer tersebut semua orang bisa dengan mudah bekerja dan hal ini bisa menghemat waktu. Dalam berbagai kebutuhan aplikasi Ms. Office ini paling banyak digunakan. Pada aplikasi ini terdapat beberapa point penting diantaranya adalah adanya aplikasi Ms. Excel, Ms. Word, Ms. Power point, Ms. Outlook dan beberapa aplikasi lainnya. Yang sering digunakan untuk perkantoran adalah Ms. Word, Ms. Office dan PowerPoint. Aplikasi ini pun menjadi bagian penting untuk mempercepat proses pengerjaan suatu pekerjaan. Selain aplikasi tersebut masih banyak lagi beberapa aplikasi yang digunakan untuk perkantoran.</p>', 'https://www.mitrawacanamedia.com/kategori-komputer-it/aplikasi-komputer', '', '2022-02-19 09:10:19', '1', '2022-02-19 09:18:44', '1'),
(6, 1, 2, 'Aplikasi Komputer Edisi 3', 'aplikasi-komputer-edisi-3', 80000, 10, 72000, 'Suharno P., Yusuf E. & Nugi N', 'Mitra Wacana Media', '2012', 'Tersedia', '1', 'aplikasi-komputer-edisi-320220219091836', '.jpg', '<p> Buku berjudul Aplikasi komputer ini sangat cocok bagi Anda yang ingin mengetahui bagaimana dan apa sebenarnya teknologi komputer dan software-software aplikasinya, khususnya bagi mahasiswa yang menempuh mata kuliah Aplikasi Komputer.    Buku ini terdiri dari 8 Bab, pembahasan awal ( Tahap Kesatu) diawali dengan 3 Bab, membahas mengenai : Pengenalan Komputer, Sistem Komputer, dan pengoperasian Dasar Windows. Pembahasan selanjutnya ( Tahap Kedua) mencakup 4 Bab, membahas mengenai paket Microsoft  office 2010 : aplikasi pengolah kata Microsoft Word 2010, aplikasi spreadsheet Microsoft Excel 2010, aplikasi presentasi Microsoft Power Point 2010 dan aplikasi pengolah database Microsoft Access 2010 Microsoft Word 2010 : merupakan program pengolah kata versi terbaru dari microsoft dengan tampilan atau fitur yang berbeda dengan versi sebelumnya. Pokok bahasan dalam buku ini adalah : Mengenal Microsoft Word 2010, Membuat dan Menyunting Dokumen, Memodifikasi Teks, Bullet dan Numbering, Bekerja Dengan Tabel, Bekerja dengan Objek, Membuat Surat Berantai ( Mail Merge), dan Mencetak Dokumen  Microsoft Excel 2010 : merupakan sistem aplikasi spreadsheet yang praktis, memiliki kemampuan menghitung dan dapat dikombinasikan dengan teks, grafik, dan diagram sehingga lebih dinamis dan interaktif. Pokok bahasan dalam buku ini adalah : Mengenal Microsoft Excel 2010, Mengatur format data dan sel,  Menggunakan Rumus dan Fungsi, Penggunaan Proteksi, Grafik dan Gambar, Database, serta Mencetak Dokumen.  Microsoft PowerPoint 2010 : merupakan aplikasi yang sangat populer dan telah banyak digunakan oleh berbagai kalangan. Pokok bahasan dalam buku ini adalah : Mengenal Microsoft PowerPoint 2010, Fungsi Pengelolaan File Presentasi, Pengeditan Slide, Penyisipan pada Slide, Penerapan Animasi Slide, Ulasan Dokumen Presentasi, dan Mencetak Dokumen.  Microsoft Access 2010 : merupakan aplikasi yang sangat populer dan telah banyak digunakan oleh berbagai kalangan. Pokok bahasan dalam buku ini adalah : 1 Bab yang mengenai microsoft access 2010 dengan materi pembahasan mencakup: pengenalan database, menjalankan MS-access 2010, Form Tabel, Report, Soal dan Tugas.  Pembahasan terakhir ( Tahapan Ketiga) mencakup 1 Bab yang membahas mengenai pengenalan Internet, dengan materi pembahasan mencakup : Pengertian Internet, Mengakses Internet, Mengenal Browsing, Fasilitas Internet, Mendayagunakan e-mail Sebagai Sarana Komunikasi Online, Mailing List dan Search Engine.</p>', 'https://www.mitrawacanamedia.com/kategori-komputer-it/aplikasi-komputer-edisi-3', '', '2022-02-19 09:11:39', '1', '2022-02-19 09:18:36', '1'),
(7, 1, 2, 'Aplikasi statistik Multi Variat dalam Penelitian Kesehatan + cd', 'aplikasi-statistik-multi-variat-dalam-penelitian-kesehatan-cd', 700000, 0, 700000, 'Dr. Stang', ',', '2017', 'Tersedia', '1', 'aplikasi-statistik-multi-variat-dalam-penelitian-kesehatan-cd20220219091827', '.jpg', '<p>Buku  Aplikasi Statistik Mutlivariat dalam Penelitian Kesehatan ini disusun untuk memenuhi kebutuhan para peneliti jika melakukan penelitian melibatkan lebih dari satu variabel bebas atau variabel tidak bebas, menganalisis pengaruh langsung atau tidak langsung, mendeteksi variabel konfounding dan interaksi, menganalisis pengaruh variabel bebas terhadap variabel tidak bebas yang berbentuk laten (konsep) dan manganalisis data yang ada kaitannya dengan efek spasial. Pendekatan buku ini adalah pendekatan yang sederhana, aplikatif yaitu analisis data semua menggunakan contoh kasus dalam penelitian kesehatan. Pengolahan dan analisis data menggunakan software SPSS, LISREL, GeoDa dan OpenBugs.  </p>', 'https://www.mitrawacanamedia.com/kategori-komputer-it/aplikasi-statistik-multi-variat-dalam-penelitian-kesehatan', '', '2022-02-19 09:14:03', '1', '2022-02-19 09:18:27', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event`
--

CREATE TABLE `tbl_event` (
  `id_event` int(5) NOT NULL,
  `judul_event` varchar(100) NOT NULL,
  `slug_event` varchar(100) NOT NULL,
  `desc_event` text,
  `foto` text,
  `foto_type` char(5) DEFAULT NULL,
  `foto_size` int(10) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_event`
--

INSERT INTO `tbl_event` (`id_event`, `judul_event`, `slug_event`, `desc_event`, `foto`, `foto_type`, `foto_size`, `created_at`) VALUES
(1, 'Lomba Cipta Cerpen - Tema Tentangmu dan Hujan - Cahaya Pelangi Media', 'lomba-cipta-cerpen-tema-tentangmu-dan-hujan-cahaya-pelangi-media', '<p>Selamat siang sahabat-sahabat literasi. Ada event cipta cerpen nih, cocok buat kalian yang sudah mempunyai hubungan dengan dia. Even Cipta Cerpen kali ini mengusung tema \"Tentangmu dan Hujan\" bersama Penerbit Cahaya Pelangi Media. Musim hujan gini, pasti punya banyak kenangan untuk dikenang dong ya, atau kemarin habis jalan sama doi terus kehujanan? Acieeee... Abadikan semua kenangan dan moment saat hujan dalam sebuah cerpen yuk ! Catat tanggal pentingnya, dan pasti reward sangat menrik.</p>\r\n<p> </p>\r\n<p><strong>Ketentuan Naskah :</strong></p>\r\n<p> </p>\r\n<ol xss=removed>\r\n<li>Karya Orisinil bukan plagiat, sesuai PUEBI, KBBI, dan EYD.</li>\r\n<li>Naskah minimal 1000 kata dan maksimal 1500 kata.</li>\r\n<li>Naskah diketik rapih A4, TNR, ukuran 12, spasi 1,5, margin normal.</li>\r\n<li>Bionarasi maksimal 50 kata, menggunakan pov 3, di halaman terakhir.</li>\r\n<li>Dikirim dalam bentuk dokumen.</li>\r\n<li>Konfirmasi pengiriman hanya pada nomor yang tertera di banner.</li>\r\n<li>Keputusan juri tidak bisa diganggu gugat.</li>\r\n<li>Wajib membeli buku minimal 1 eksemplar.</li>\r\n</ol>\r\n<p> </p>\r\n<p><strong>Reward:</strong></p>\r\n<p> </p>\r\n<ul xss=removed>\r\n<li>Juara 1, 2, 3 mendapat Piala, Sertifikat cetak, Ganci dan Souvenir.</li>\r\n<li>Seluruh kontributor mendapat piala dan sertifikat cetak.</li>\r\n<li>Semua karya dibukukan.</li>\r\n<li>Buku ber-ISBN.</li>\r\n</ul>\r\n<p> </p>\r\n<p><strong>Timeline :</strong></p>\r\n<p> </p>\r\n<ul xss=removed>\r\n<li>Pendaftaran :10 Feb - 10 Maret 2021.</li>\r\n<li>Deadline naskah : 20 Maret 2021.</li>\r\n</ul>\r\n<p>Masih banyak waktu yuk kak, daftar dari sekarang sebelum kuota terpenuhi!!</p>', 'lomba-cipta-cerpen-tema-tentangmu-dan-hujan-cahaya-pelangi-media20220219094442', '.jpg', NULL, '2022-02-19 09:47:12'),
(2, 'Lomba Cipta Cerpen Nasional - Total Hadiah 2 Juta Rupiah - Fun Bahasa', 'lomba-cipta-cerpen-nasional-total-hadiah-2-juta-rupiah-fun-bahasa', '<p>Halo Sahabat Sastrawan, Ada Event Lomba Cipta Cerita Pendek Nasional. Lomba Kali Ini Mengusung Tema Bebas atau tidak ditentukan. Bagi Anda Yang Berminat Mengikuti Event Kali Ini Silahkan Catat Tanggal - Tanggal Pentingnya Agar Tidak Ketinggalan. Mari tuangkan perasaan kita melalui cerita pendek dengan mengikuti Festival Cipta Cerpen Nasional 2021 #FCCN2021 dari Fun Bahasa. Tulis dan jadikan karyamu menjadi luar biasa.</p>\r\n<p><strong>Hadiah Utama :</strong></p>\r\n<p> </p>\r\n<ol xss=removed>\r\n<li>Uang Tunai total 2jt</li>\r\n<li>Sertifikat Cetak (10 besar)</li>\r\n<li>Tas Jinjing Fun Bahasa</li>\r\n<li>Blocknote ekslusif</li>\r\n<li>Buku Antologi ber-ISBN</li>\r\n</ol>\r\n<p> </p>\r\n<p><strong>Reward :</strong></p>\r\n<p> </p>\r\n<ul xss=removed>\r\n<li>Juara 1 : 1 Unit Smartphone + Rp. 1.000.000,- + Buku Antologi Cetak</li>\r\n<li>Juara 2 : Rp. 1.000.000,- + Buku Antologi Cetak</li>\r\n<li>Juara 3 : Rp. 500.000,- + Buku Antologi Cetak</li>\r\n<li>Untuk 3 Juara Favorit : Rp. 100.000,- + Buku Antologi Cetak</li>\r\n</ul>', 'lomba-cipta-cerpen-nasional-total-hadiah-2-juta-rupiah-fun-bahasa20220219095110', '.jpg', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis`
--

CREATE TABLE `tbl_jenis` (
  `id_jenis` int(5) NOT NULL,
  `nama_jenis` varchar(25) DEFAULT NULL,
  `slug_jenis` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_jenis`
--

INSERT INTO `tbl_jenis` (`id_jenis`, `nama_jenis`, `slug_jenis`) VALUES
(1, 'Buku', 'buku');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `id_kategori` int(5) NOT NULL,
  `nama_kategori` varchar(25) DEFAULT NULL,
  `id_jenis` int(5) NOT NULL,
  `slug_kategori` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `nama_kategori`, `id_jenis`, `slug_kategori`) VALUES
(2, 'Ilmu Komputer', 1, 'ilmu-komputer');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_level`
--

CREATE TABLE `tbl_level` (
  `id_level` int(5) NOT NULL,
  `nama_level` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_level`
--

INSERT INTO `tbl_level` (`id_level`, `nama_level`) VALUES
(1, 'Superadmin'),
(2, 'Admin'),
(3, 'Penulis');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id_slider` int(5) NOT NULL,
  `nomor_urut` int(5) DEFAULT NULL,
  `judul_slider` varchar(50) DEFAULT NULL,
  `link` text,
  `foto` text,
  `foto_type` char(5) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` char(15) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_by` char(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id_slider`, `nomor_urut`, `judul_slider`, `link`, `foto`, `foto_type`, `created_at`, `created_by`, `update_at`, `update_by`) VALUES
(1, 1, 'Kumpulan Buku Keren', 'http://localhost/git/kulitulis/', '120220216230511', '.jpg', '2022-02-16 23:05:11', NULL, NULL, NULL),
(2, 2, 'Building Novel Romance', 'http://localhost/git/kulitulis/', '220220216230536', '.jpg', '2022-02-16 23:05:36', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tulisan`
--

CREATE TABLE `tbl_tulisan` (
  `id_tulisan` char(15) NOT NULL,
  `id_jenis` int(5) NOT NULL,
  `id_kategori` int(5) NOT NULL,
  `id_user` int(5) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `sinopsis` text,
  `tulisan` text,
  `tgl_kirim` date DEFAULT NULL,
  `status` char(10) DEFAULT 'P',
  `tgl_disetujui` date DEFAULT NULL,
  `flag_bayar` char(1) DEFAULT 'N',
  `tgl_bayar` date DEFAULT NULL,
  `note` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` char(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_tulisan`
--

INSERT INTO `tbl_tulisan` (`id_tulisan`, `id_jenis`, `id_kategori`, `id_user`, `judul`, `sinopsis`, `tulisan`, `tgl_kirim`, `status`, `tgl_disetujui`, `flag_bayar`, `tgl_bayar`, `note`, `created_at`, `created_by`) VALUES
('20220326084738', 1, 2, 16, 'tulisan 1', '<p>sinopsis 1</p>', '<p>tes aja adanaj ada</p>', '2022-03-26', 'R', NULL, 'N', '0000-00-00', NULL, '2022-03-26 08:47:58', 'FN'),
('20220326084804', 1, 2, 16, 'tulisan 2', '<p>sinopsis</p>', '<h1 id=\"headline-34-99112\" class=\"ct-headline\"><span id=\"span-35-99112\" class=\"ct-span\">20 HP iPhone Terbaru dan Terbaik di Tahun 2022</span></h1>\r\n<div id=\"text_block-32-99112\" class=\"ct-text-block\">\r\n<p>Sejak awal kemunculannya hingga sekarang, Apple selalu meluncurkan <em>flagship</em> yang mampu menjadi <em>trendsetter</em> di dunia <em>smartphone</em>. Di awali dengan munculnya iPhone 3Gs beberapa tahun silam, Apple mampu menjawab kebutuhan masyarakat akan hadirnya <em>smartphone</em> dengan inovasi teknologi yang mumpuni.</p>\r\n<p>Hingga kini, Apple telah merilis banyak unit <em>smartphone</em> dengan beragam jenis model dan tentu saja spesifikasi yang berbeda-beda. <em>Nah</em>, <a href=\"https://carisinyal.com/\">Carisinyal</a> akan merangkum deretan HP iPhone terbaru yang baraangkali cocok untuk Anda pilih. Mari simak langsung saja pembahasannya di bawah ini.</p>\r\n<h2>1. Apple iPhone SE 2022 </h2>\r\n</div>\r\n<div class=\"ct-text-block\"> </div>\r\n<div class=\"ct-text-block\">\r\n<p> </p>\r\n<h1 id=\"headline-34-99112\" class=\"ct-headline\"><span id=\"span-35-99112\" class=\"ct-span\">20 HP iPhone Terbaru dan Terbaik di Tahun 2022</span></h1>\r\n<div id=\"text_block-32-99112\" class=\"ct-text-block\">\r\n<p>Sejak awal kemunculannya hingga sekarang, Apple selalu meluncurkan <em>flagship</em> yang mampu menjadi <em>trendsetter</em> di dunia <em>smartphone</em>. Di awali dengan munculnya iPhone 3Gs beberapa tahun silam, Apple mampu menjawab kebutuhan masyarakat akan hadirnya <em>smartphone</em> dengan inovasi teknologi yang mumpuni.</p>\r\n<p>Hingga kini, Apple telah merilis banyak unit <em>smartphone</em> dengan beragam jenis model dan tentu saja spesifikasi yang berbeda-beda. <em>Nah</em>, <a href=\"https://carisinyal.com/\">Carisinyal</a> akan merangkum deretan HP iPhone terbaru yang barangkali cocok untuk Anda pilih. Mari simak langsung saja pembahasannya di bawah ini.</p>\r\n<h2>1. Apple iPhone SE 2022</h2>\r\n</div>\r\n<div class=\"ct-text-block\">\r\n<table class=\"box-info\">\r\n<tbody>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Layar</td>\r\n<td class=\"kolom-dua\">Retina IPS LCD 4.7 inci</td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Chipset</td>\r\n<td class=\"kolom-dua\">Apple A15 Bionic</td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">RAM</td>\r\n<td class=\"kolom-dua\">4 GB</td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Memori Internal</td>\r\n<td class=\"kolom-dua\">64 GB, 128 GB, 256 GB</td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Kamera</td>\r\n<td class=\"kolom-dua\"><span class=\"pemeran-column-dua\"> 12 MP (wide) </span></td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Baterai</td>\r\n<td class=\"kolom-dua\">Li-Ion - mAh</td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Harga Saat Rilis</td>\r\n<td class=\"kolom-dua\">Rp -</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<table class=\"box-info\">\r\n<tbody>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Layar</td>\r\n<td class=\"kolom-dua\">Retina IPS LCD 4.7 inci</td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Chipset</td>\r\n<td class=\"kolom-dua\">Apple A15 Bionic</td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">RAM</td>\r\n<td class=\"kolom-dua\">4 GB  </td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Memori Internal</td>\r\n<td class=\"kolom-dua\">64 GB, 128 GB, 256 GB</td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Kamera</td>\r\n<td class=\"kolom-dua\"><span class=\"pemeran-column-dua\"> 12 MP (wide) </span></td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Baterai</td>\r\n<td class=\"kolom-dua\">Li-Ion - mAh</td>\r\n</tr>\r\n<tr class=\"box-baris\">\r\n<td class=\"kolom-satu\">Harga Saat Rilis</td>\r\n<td class=\"kolom-dua\">Rp -</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '2022-03-26', 'A', NULL, 'Y', '2022-03-26', NULL, '2022-03-26 08:50:03', 'FN'),
('20220326100235', 1, 2, 16, 'aadad', '<p>adadad adad d</p>', '<p>q dq q qdq q</p>', '2022-03-26', 'A', '2022-03-26', 'Y', '2022-03-26', NULL, '2022-03-26 10:02:44', 'FN'),
('20220326100251', 1, 2, 16, 'adadad', '<p>adadad ad a a a a</p>', '<p> a afa a aa a a</p>', '2022-03-26', 'R', NULL, 'N', '0000-00-00', 'Banyak Typo', '2022-03-26 10:03:01', 'FN'),
('20220327012225', 1, 2, 17, 'adadaad', '<p>adad</p>', '<p>adadaadadadadadad ada ad ac</p>', '0000-00-00', 'D', NULL, 'N', '0000-00-00', NULL, '2022-03-27 13:22:39', 'Nada'),
('20220327012246', 1, 2, 17, 'adadada', '<p>adada ad ad</p>', '<p>adad ada ad ada ad ad ad ad</p>', '0000-00-00', 'D', NULL, 'N', '0000-00-00', NULL, '2022-03-27 13:23:02', 'Nada'),
('20220327031827', 1, 2, 16, 'draft 1', '<p>adad a</p>', '<p>adadd adad adda ada</p>', '0000-00-00', 'D', NULL, 'N', '0000-00-00', NULL, '2022-03-27 15:19:35', 'FN'),
('20220327031851', 1, 2, 16, 'draf 2', '<p>adad</p>', '<p>adadad a a s s s</p>', '2022-03-27', 'S', NULL, 'N', '0000-00-00', NULL, '2022-03-27 15:19:02', 'FN'),
('20220327031909', 1, 2, 16, 'draf 2', '<p>da a a a s a a</p>', '<p>a a a a a a</p>', '0000-00-00', 'D', NULL, 'N', '0000-00-00', NULL, '2022-03-27 15:19:23', 'FN'),
('20220327124420', 1, 2, 17, 'tulisan 1', '<p>adadadad</p>', '<p>adadadacadad a ada ada ada a</p>', '0000-00-00', 'D', NULL, 'N', '0000-00-00', NULL, '2022-03-27 12:44:41', 'Nada'),
('20220327124444', 1, 2, 17, 'kirim', '<p>adadadad</p>', '<p>adada ada ada ada</p>', '2022-03-27', 'S', NULL, 'N', '0000-00-00', NULL, '2022-03-27 12:44:55', 'Nada');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(5) NOT NULL,
  `id_level` int(5) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `nama_pena` varchar(25) DEFAULT NULL,
  `nomor_hp` char(15) DEFAULT NULL,
  `nama_bank` varchar(15) DEFAULT NULL,
  `nomor_rekening` varchar(15) DEFAULT NULL,
  `atas_nama` varchar(50) DEFAULT NULL,
  `tentang_saya` text,
  `foto` text NOT NULL,
  `foto_type` char(5) NOT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `id_level`, `email`, `password`, `nama_lengkap`, `nama_pena`, `nomor_hp`, `nama_bank`, `nomor_rekening`, `atas_nama`, `tentang_saya`, `foto`, `foto_type`, `active`, `ip_address`, `salt`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `last_login`, `created_at`, `update_at`) VALUES
(1, 1, 'superadmin@gmail.com', '$2y$08$D8i7zeJSUQulCZKsx/FKGul8ZwWcBxcCGL34/J6Ipzs228xyS9TSG', 'Superadmin', 'Superadmin', '123721378172', 'bri', '12312398', 'sadashdkj', 'asdasdasdasdasds', '20220306201445', '.jpg', 1, ':1', NULL, NULL, 'HhFOZk3TMjY6njEY8wfBiea50e1717cfd64c6435', 1519646514, 'Zd7Wp.SNfi0t.EHZWhEONe', 1648367573, '2022-02-05 11:28:10', '2022-03-06 20:23:43'),
(15, 2, 'admin@gmail.com', '$2y$08$CcaeU7nFnvgjUay41ZtwUOmtCESShMATn1ErLnM0Q3.zXjX0iVXDC', 'admin', 'admin', '0', '', '', '', NULL, '', '', 1, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, 1648386296, '2022-02-06 15:14:20', NULL),
(16, 3, 'fahminurcahya@gmail.com', '$2y$08$aFS1doRhQWKfMVrdfXgKN.Cicd4H1BrHTV0E/dCHO03UHBx291DCG', 'Fahmi Nurcahya', 'FN', '088883', 'BCA', '098765', 'Fahmi Nurcahya', 'adadadda', '20220327150615', '.jpg', 1, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, 1648368845, '2022-02-06 15:36:53', '2022-03-27 15:13:50'),
(17, 3, 'nada@gmail.com', '$2y$08$wViteuW/k.uWoKqJKRW1aOP7IwKz1MhyGqeU0vC0LVJENVbBKuz8C', 'nada syasita', 'Nada', '08787867575757', 'BRI', '817210296', 'Nada', 'lulusan ipb', '20220327115842', '.png', 1, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, 1648356293, '2022-03-27 11:44:39', '2022-03-27 13:47:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_company`
--
ALTER TABLE `tbl_company`
  ADD PRIMARY KEY (`id_company`);

--
-- Indexes for table `tbl_ebook`
--
ALTER TABLE `tbl_ebook`
  ADD PRIMARY KEY (`id_ebook`) USING BTREE;

--
-- Indexes for table `tbl_event`
--
ALTER TABLE `tbl_event`
  ADD PRIMARY KEY (`id_event`);

--
-- Indexes for table `tbl_jenis`
--
ALTER TABLE `tbl_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id_kategori`),
  ADD KEY `relasi_jenis_kategori` (`id_jenis`);

--
-- Indexes for table `tbl_level`
--
ALTER TABLE `tbl_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `tbl_tulisan`
--
ALTER TABLE `tbl_tulisan`
  ADD PRIMARY KEY (`id_tulisan`),
  ADD KEY `relasi_jenis_tulisan` (`id_jenis`),
  ADD KEY `relasi_kategori_tulisan` (`id_kategori`),
  ADD KEY `relasi_user_tulisan` (`id_user`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`,`email`),
  ADD KEY `relasi_level_user` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_company`
--
ALTER TABLE `tbl_company`
  MODIFY `id_company` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_ebook`
--
ALTER TABLE `tbl_ebook`
  MODIFY `id_ebook` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_event`
--
ALTER TABLE `tbl_event`
  MODIFY `id_event` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_jenis`
--
ALTER TABLE `tbl_jenis`
  MODIFY `id_jenis` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id_kategori` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_level`
--
ALTER TABLE `tbl_level`
  MODIFY `id_level` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id_slider` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD CONSTRAINT `relasi_jenis_kategori` FOREIGN KEY (`id_jenis`) REFERENCES `tbl_jenis` (`id_jenis`);

--
-- Constraints for table `tbl_tulisan`
--
ALTER TABLE `tbl_tulisan`
  ADD CONSTRAINT `relasi_jenis_tulisan` FOREIGN KEY (`id_jenis`) REFERENCES `tbl_jenis` (`id_jenis`),
  ADD CONSTRAINT `relasi_kategori_tulisan` FOREIGN KEY (`id_kategori`) REFERENCES `tbl_kategori` (`id_kategori`),
  ADD CONSTRAINT `relasi_user_tulisan` FOREIGN KEY (`id_user`) REFERENCES `tbl_user` (`id_user`);

--
-- Constraints for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD CONSTRAINT `relasi_level_user` FOREIGN KEY (`id_level`) REFERENCES `tbl_level` (`id_level`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
