<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function index()
    {
        $this->load->model('EventModel');
        $this->load->model('CompanyModel');
        $this->load->model('EbookModel');
        $this->load->model('SliderModel');

        $this->data['title']                             = 'Home';

        $this->data['company_data']             = $this->CompanyModel->get_by_company();
        $this->data['ebook_new_data']         = $this->EbookModel->get_all_new_home();
        $this->data['ebook_best_data']         = $this->EbookModel->get_all_best_home();
        $this->data['event_new_data']             = $this->EventModel->get_all_new_home();
        $this->data['slider_data']                 = $this->SliderModel->get_all_home();

        $this->load->view('front/home/body', $this->data);
    }
}
