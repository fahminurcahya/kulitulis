<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penulis extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('PenulisModel');
        $this->load->model('JenisModel');
        $this->load->model('KategoriModel');
        $this->load->model('UsersModel');


        $this->data['module'] = 'Penulis';

        if (!$this->ion_auth->is_penulis()) {
            redirect(base_url());
        }
        // elseif($this->ion_auth->is_user()){redirect('admin/auth/login', 'refresh');}
    }

    public function create()
    {
        $this->data['title']          = 'Tulisan Baru';
        // $this->data['action']         = site_url('admin/Penulis/publish');
        $this->data['button_submit']  = 'Publish';
        $this->data['button_draft']   = 'Draft';
        $id_user = $_SESSION['id_user'];
        $id_tulisan = date('Ymdhis');

        $this->data['id_user'] = array(
            'name'  => 'id_user',
            'id'    => 'id_user',
            'type'  => 'hidden',
            'value' => $id_user,
        );

        $this->data['id_tulisan'] = array(
            'name'  => 'id_tulisan',
            'id'    => 'id_tulisan',
            'type'  => 'hidden',
            'value' => $id_tulisan,
        );

        $this->data['judul'] = array(
            'name'  => 'judul',
            'id'    => 'judul',
            'class' => 'form-control',
        );

        $this->data['id_jenis'] = array(
            'name'        => 'id_jenis',
            'id'          => 'id_jenis',
            'class'       => 'form-control',
            'onChange'    => 'tampilKategori()',
            'required'    => '',
        );
        $this->data['id_kategori'] = array(
            'name'        => 'id_kategori',
            'id'          => 'id_kategori',
            'class'       => 'form-control',
            'required'    => '',
        );

        $this->data['sinopsis'] = array(
            'name'  => 'sinopsis',
            'id'    => 'sinopsis',
            'class' => 'form-control',
            'rows'  => '2',
        );

        $this->data['tulisan'] = array(
            'name'  => 'tulisan',
            'id'    => 'tulisan',
            'class' => 'form-control',
        );

        $this->data['nama_pena'] = array(
            'name'  => 'nama_pena',
            'id'    => 'nama_pena',
            'class' => 'form-control',
            'value' => $this->UsersModel->get_nama_pena($id_user),
            'disabled' => 'disabled',
        );

        $this->data['ambil_jenis'] = $this->JenisModel->ambil_jenis();
        $this->load->view('back/penulis/create', $this->data);
    }

    public function pilih_kategori()
    {
        $this->data['kategori'] = $this->KategoriModel->ambil_kategori($this->uri->segment(4));
        $this->load->view('back/penulis/v_kategori', $this->data);
    }

    public function publish()
    {
        $this->_rules();
        $id_user = $_SESSION['id_user'];

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_user'));
        } else {
            $id['id_user'] = $this->input->post('id_user');

            $data = array(
                'id_tulisan'      => $this->input->post('id_tulisan'),
                'id_jenis'      => $this->input->post('id_jenis'),
                'id_kategori'   => $this->input->post('id_kategori'),
                'id_user'     => $this->input->post('id_user'),
                'judul'    => $this->input->post('judul'),
                'sinopsis'       => $this->input->post('sinopsis'),
                'tulisan'       => $this->input->post('tulisan'),
                'tgl_kirim' => date('Y-m-d'),
                'status'       => 'S',
                'flag_bayar'       => 'N',
                'tgl_bayar'       => '',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->UsersModel->get_nama_pena($id_user)
            );

            $this->PenulisModel->publish($this->input->post('id_user'), $data);
            $this->session->set_flashdata('message', '
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
          <i class="ace-icon fa fa-bullhorn green"></i> Tulisan Berhasil Dikirim
        </div>');
            redirect(site_url('tulisan/list/dikirim'));
        }
    }

    public function draft()
    {
        $this->_rules();
        $id_user = $_SESSION['id_user'];

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_user'));
        } else {
            $id['id_user'] = $this->input->post('id_user');

            $data = array(
                'id_tulisan'        => $this->input->post('id_tulisan'),
                'id_jenis'          => $this->input->post('id_jenis'),
                'id_kategori'       => $this->input->post('id_kategori'),
                'id_user'           => $this->input->post('id_user'),
                'judul'             => $this->input->post('judul'),
                'sinopsis'          => $this->input->post('sinopsis'),
                'tulisan'           => $this->input->post('tulisan'),
                'tgl_kirim'         => '',
                'status'            => 'D',
                'flag_bayar'        => 'N',
                'tgl_bayar'         => '',
                'created_at'        => date('Y-m-d H:i:s'),
                'created_by'        => $this->UsersModel->get_nama_pena($id_user)
            );

            $this->PenulisModel->draft($this->input->post('id_user'), $data);
            $this->session->set_flashdata('message', '
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
          <i class="ace-icon fa fa-bullhorn green"></i> Tulisan Berhasil Disimpan
        </div>');
            redirect(site_url('tulisan/list/draft_list'));
        }
    }

    public function list($status)
    {
        if ($status == 'draft_list') {
            $this->data['title'] = "List Draft";
        } else if ($status == 'dikirim') {
            $this->data['title'] = "List Tulisan Dikirim";
        }
        $this->load->view('back/penulis/' . $status, $this->data);
    }

    public function ajax_list($status)
    {
        //get_datatables terletak di model
        if ($status == 'draft_list') {
            $status = 'D';
        } else if ($status == 'dikirim') {
            $status = 'S';
        }

        $list = $this->PenulisModel->get_datatables($status);
        $data = array();
        $no = $_POST['start'];

        // Membuat loop/ perulangan
        foreach ($list as $data_tulisan) {

            $no++;
            $row = array();
            $row[] = '<p style="text-align: center">' . $no . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->judul . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_jenis . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_kategori . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_lengkap . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_pena . '</p>';
            if ($status == 'S') {
                $row[] = '<p style="text-align: left">' . $data_tulisan->tgl_kirim . '</p>';
            }
            if ($data_tulisan->status == 'D') {
                $sta = "Draft";
            } else if ($data_tulisan->status == 'S') {
                $sta = "Dikirim";
            } else if ($data_tulisan->status == 'A' && $data_tulisan->flag_bayar != 'Y') {
                $sta = "Disetujui";
            } else if ($data_tulisan->status == 'R') {
                $sta = "Ditolak";
            } else if ($data_tulisan->status == 'A' && $data_tulisan->flag_bayar == 'Y') {
                $sta = "Dibayar";
            } else {
                $sta = "";
            }
            $row[] = '<p style="text-align: left">' . $sta . '</p>';

            if ($status == 'D') {
                $row[] = '
                <p style="text-align: center">
                  <a class="btn btn-sm btn-warning" href="' . base_url('tulisan/draft_edit/') . $data_tulisan->id_tulisan . '" title="Lihat"><i class="fa fa-edit"> Edit</i></a>
                  <a class="btn btn-sm btn-danger" href="' . base_url('tulisan/draft_delete/') . $data_tulisan->id_tulisan . '" title="Lihat"><i class="fa fa-trash"> Delete</i></a>
                </p>';
            }

            // Penambahan tombol edit dan hapus


            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->PenulisModel->count_all(),
            "recordsFiltered" => $this->PenulisModel->count_filtered($status),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    public function draft_edit($id_tulisan)
    {
        $this->data['title']          = 'Tulisan';
        $this->data['button_submit']  = 'Publish';
        $this->data['button_draft']   = 'Draft';
        $id_user = $_SESSION['id_user'];

        $row = $this->PenulisModel->get_by_id($id_tulisan);
        $this->data['penulis'] = $this->PenulisModel->get_by_id($id_tulisan);

        if ($row) {
            $this->data['id_user'] = array(
                'name'  => 'id_user',
                'id'    => 'id_user',
                'type'  => 'hidden',
                'value' => $id_user,
            );

            $this->data['id_tulisan'] = array(
                'name'  => 'id_tulisan',
                'id'    => 'id_tulisan',
                'type'  => 'hidden',
                'value' => $id_tulisan,
            );

            $this->data['judul'] = array(
                'name'  => 'judul',
                'id'    => 'judul',
                'class' => 'form-control',
            );

            $this->data['id_jenis'] = array(
                'name'        => 'id_jenis',
                'id'          => 'id_jenis',
                'class'       => 'form-control',
                'onChange'    => 'tampilKategori()',
                'required'    => '',
            );
            $this->data['id_kategori'] = array(
                'name'        => 'id_kategori',
                'id'          => 'id_kategori',
                'class'       => 'form-control',
                'required'    => '',
            );

            $this->data['sinopsis'] = array(
                'name'  => 'sinopsis',
                'id'    => 'sinopsis',
                'class' => 'form-control',
                'rows'  => '2',
            );

            $this->data['tulisan'] = array(
                'name'  => 'tulisan',
                'id'    => 'tulisan',
                'class' => 'form-control',
            );

            $this->data['nama_pena'] = array(
                'name'  => 'nama_pena',
                'id'    => 'nama_pena',
                'class' => 'form-control',
                'value' => $this->UsersModel->get_nama_pena($id_user),
                'disabled' => 'disabled',
            );

            $this->data['ambil_jenis'] = $this->JenisModel->ambil_jenis();
            $jenis = $row->id_jenis;
            $this->data['ambil_kategori']       = $this->KategoriModel->ambil_kategori($jenis);
            if ($this->data['penulis']->note != '') {
                $this->session->set_flashdata('warning', '
                  <div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                    <i class="ace-icon fa fa-bullhorn green"></i> ' . $this->data['penulis']->note . '
                  </div>');
            }
            $this->load->view('back/penulis/draft_edit', $this->data);
        } else {
            $this->session->set_flashdata('message', '
          <div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
            <i class="ace-icon fa fa-bullhorn green"></i> Data tidak ditemukan
          </div>');
            redirect(site_url('tulisan/list/draft_list'));
        }
    }

    public function draft_delete($id)
    {
        $delete = $this->PenulisModel->delete($id);

        $this->session->set_flashdata('message', '
            <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
              <i class="ace-icon fa fa-bullhorn green"></i> Tulisan berhasil dihapus
            </div>');
        redirect(site_url('tulisan/list/draft_list'));
    }

    public function _rules()
    {
        $this->form_validation->set_rules('judul', 'Judul Tulisan', 'trim|required');

        // set pesan form validasi error
        $this->form_validation->set_message('required', '{field} wajib diisi');

        $this->form_validation->set_rules('id_tulisan', 'id_tulisan', 'trim');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert">', '</div>');
    }
}
