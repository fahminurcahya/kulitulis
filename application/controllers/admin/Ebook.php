<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ebook extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('EbookModel');
        $this->load->model('JenisModel');
        $this->load->model('KategoriModel');


        $this->data['module'] = 'Ebook';

        if (!$this->ion_auth->is_superadmin() && !$this->ion_auth->is_admin()) {
            redirect(base_url());
        }
        // elseif($this->ion_auth->is_user()){redirect('admin/auth/login', 'refresh');}
    }

    public function index()
    {
        $this->data['title'] = "Data Ebook";
        $this->load->view('back/ebook/index', $this->data);
    }

    public function ajax_list()
    {
        //get_datatables terletak di model
        $list = $this->EbookModel->get_datatables();
        $data = array();
        $no = $_POST['start'];

        // Membuat loop/ perulangan
        foreach ($list as $data_ebook) {
            $no++;
            $row = array();
            $row[] = '<p style="text-align: center">' . $no . '</p>';
            if ($data_ebook->best_seller == '1') {
                $row[] = "<input type='checkbox' id=" . $data_ebook->id_ebook . " name='update' onchange='bestSeller(this)' style='zoom: 2.0; text-align: center;' checked>";
            } else {
                $row[] = "<input type='checkbox' id=" . $data_ebook->id_ebook . " name='update' onchange='bestSeller(this)' style='zoom: 2.0; text-align: center;'>";
            }
            $row[] = '<p style="text-align: left">' . $data_ebook->judul . '</p>';
            $row[] = '<p style="text-align: center"><img src="' . base_url('assets/images/ebook/') . $data_ebook->foto . $data_ebook->foto_type . '" width="100px"></p>';
            $row[] = '<p style="text-align: center">' . $data_ebook->stok . '</p>';


            // Penambahan tombol edit dan hapus
            $row[] = '
        <p style="text-align: center">
          <a class="btn btn-sm btn-warning" href="' . base_url('admin/ebook/update/') . $data_ebook->id_ebook . '" title="Edit"><i class="fa fa-pencil"></i></a>
          <a class="btn btn-sm btn-danger" href="' . base_url('admin/ebook/delete/') . $data_ebook->id_ebook . '" title="Hapus" onclick="javasciprt: return confirm(\'Apakah Anda yakin ?\')"><i class="glyphicon glyphicon-remove"></i></a>
        </p>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->EbookModel->count_all(),
            "recordsFiltered" => $this->EbookModel->count_filtered(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_best()
    {
        //get_datatables terletak di model
        $list = $this->EbookModel->get_datatables_best();
        $data = array();
        $no = 0;

        // Membuat loop/ perulangan
        foreach ($list as $data_ebook) {
            $no++;
            $row = array();
            $row[] = '<p style="text-align: left">' . $no . '</p>';
            $row[] = '<p style="text-align: left">' . $data_ebook->judul . '</p>';
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function create()
    {
        $this->data['title']          = 'Tambah Data ' . $this->data['module'];
        $this->data['action']         = site_url('admin/ebook/create_action');
        $this->data['button_submit']  = 'Simpan';
        $this->data['button_reset']   = 'Reset';

        $this->data['judul'] = array(
            'name'  => 'judul',
            'id'    => 'judul',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('judul'),
        );

        $this->data['id_jenis'] = array(
            'name'        => 'id_jenis',
            'id'          => 'id_jenis',
            'class'       => 'form-control',
            'onChange'    => 'tampilKategori()',
            'required'    => '',
        );
        $this->data['id_kategori'] = array(
            'name'        => 'id_kategori',
            'id'          => 'id_kategori',
            'class'       => 'form-control',
            'required'    => '',
        );

        $this->data['harga_normal'] = array(
            'name'  => 'harga_normal',
            'id'    => 'b',
            'class' => 'form-control',
            'placeholder'    => 'Isikan angka saja',
            'value' => $this->form_validation->set_value('harga_normal'),
            'onkeyup'            => 'hitung();',
        );

        $this->data['diskon'] = array(
            'name'  => 'diskon',
            'id'    => 'a',
            'class' => 'form-control',
            'placeholder'    => 'Isikan angka saja',
            'value' => $this->form_validation->set_value('diskon'),
            'onkeyup'            => 'hitung();',
        );

        $this->data['harga_diskon'] = array(
            'name'  => 'harga_diskon',
            'id'    => 'd',
            'class' => 'form-control',
            'placeholder'    => 'Isikan angka saja',
            'value' => $this->form_validation->set_value('harga_diskon'),
            'onkeyup'            => 'hitung();',
        );

        $this->data['penulis'] = array(
            'name'  => 'penulis',
            'id'    => 'penulis',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('penulis'),
        );

        $this->data['penerbit'] = array(
            'name'  => 'penerbit',
            'id'    => 'penerbit',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('penerbit'),
        );

        $this->data['tahun_terbit'] = array(
            'name'  => 'tahun_terbit',
            'id'    => 'tahun_terbit',
            'class' => 'form-control',
            'placeholder'    => 'Isikan tahun saja',
            'value' => $this->form_validation->set_value('tahun_terbit'),
        );

        $this->data['stok'] = array(
            'name'        => 'stok',
            'id'          => 'stok',
            'class'       => 'form-control',
            'required'    => '',
        );

        $this->data['sinopsis'] = array(
            'name'  => 'sinopsis',
            'id'    => 'sinopsis',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('sinopsis'),
        );

        $this->data['link_tokped'] = array(
            'name'  => 'link_tokped',
            'id'    => 'link_tokped',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('link_tokped'),
        );

        $this->data['link_shopee'] = array(
            'name'  => 'link_shopee',
            'id'    => 'link_shopee',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('link_shopee'),
        );

        $pilihanstok['Tersedia'] = 'Tersedia';
        $pilihanstok['Tidak Tersedia'] = 'Tidak Tersedia';
        $this->data['ambil_stok']     = $pilihanstok;
        $this->data['ambil_jenis'] = $this->JenisModel->ambil_jenis();
        $this->load->view('back/ebook/ebook_add', $this->data);
    }

    public function pilih_kategori()
    {
        $this->data['kategori'] = $this->KategoriModel->ambil_kategori($this->uri->segment(4));
        $this->load->view('back/ebook/v_kategori', $this->data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            /* 4 adalah menyatakan tidak ada file yang diupload*/
            if ($_FILES['foto']['error'] <> 4) {
                $nmfile = strtolower(url_title($this->input->post('judul'))) . date('YmdHis');

                /* memanggil library upload ci */
                $config['upload_path']      = './assets/images/ebook/';
                $config['allowed_types']    = 'jpg|jpeg|png|gif';
                $config['max_size']         = '2048'; // 2 MB
                $config['file_name']        = $nmfile; //nama yang terupload nantinya

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('foto')) {
                    //file gagal diupload -> kembali ke form tambah
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert">' . $error['error'] . '</div>');

                    $this->create();
                }
                //file berhasil diupload -> lanjutkan ke query INSERT
                else {
                    $foto = $this->upload->data();
                    $thumbnail                = $config['file_name'];
                    // library yang disediakan codeigniter
                    $config['image_library']  = 'gd2';
                    // gambar yang akan disimpan thumbnail
                    $config['source_image']   = './assets/images/ebook/' . $foto['file_name'] . '';
                    // membuat thumbnail
                    $config['create_thumb']   = TRUE;
                    // rasio resolusi
                    $config['maintain_ratio'] = FALSE;
                    // lebar
                    $config['width']          = 310;
                    // tinggi
                    $config['height']         = 400;

                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $data = array(
                        'judul'    => $this->input->post('judul'),
                        'slug_ebook'     => strtolower(url_title($this->input->post('judul'))),
                        'id_jenis'          => $this->input->post('id_jenis'),
                        'id_kategori'       => $this->input->post('id_kategori'),
                        'harga_normal'       => $this->input->post('harga_normal'),
                        'diskon'       => $this->input->post('diskon'),
                        'harga_diskon'       => $this->input->post('harga_diskon'),
                        'penulis'       => $this->input->post('penulis'),
                        'penerbit'       => $this->input->post('penerbit'),
                        'tahun_terbit'       => $this->input->post('tahun_terbit'),
                        'stok'       => $this->input->post('stok'),
                        'sinopsis'      => $this->input->post('sinopsis'),
                        'link_tokped'      => $this->input->post('link_tokped'),
                        'link_shopee'      => $this->input->post('link_shopee'),
                        'foto'          => $nmfile,
                        'foto_type'     => $foto['file_ext'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $_SESSION["id_user"]
                    );

                    // eksekusi query INSERT
                    $this->EbookModel->insert($data);
                    // set pesan data berhasil disimpan
                    $this->session->set_flashdata('message', '
            <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
              <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil disimpan
            </div>');
                    redirect(site_url('admin/ebook'));
                }
            } else // Jika file upload kosong
            {
                $data = array(
                    'judul'  => $this->input->post('judul'),
                    'id_jenis'          => $this->input->post('id_jenis'),
                    'id_kategori'       => $this->input->post('id_kategori'),
                    'slug_ebook'     => strtolower(url_title($this->input->post('judul'))),
                    'harga_normal'       => $this->input->post('harga_normal'),
                    'diskon'       => $this->input->post('diskon'),
                    'harga_diskon'       => $this->input->post('harga_diskon'),
                    'penulis'       => $this->input->post('penulis'),
                    'penerbit'       => $this->input->post('penerbit'),
                    'tahun_terbit'       => $this->input->post('tahun_terbit'),
                    'stok'       => $this->input->post('stok'),
                    'sinopsis'      => $this->input->post('sinopsis'),
                    'link_tokped'      => $this->input->post('link_tokped'),
                    'link_shopee'      => $this->input->post('link_shopee'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $_SESSION["id_user"]
                );

                // eksekusi query INSERT
                $this->EbookModel->insert($data);
                // set pesan data berhasil disimpan
                $this->session->set_flashdata('message', '
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
          <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil disimpan
        </div>');
                redirect(site_url('admin/ebook'));
            }
        }
    }

    public function update($id)
    {
        $row = $this->EbookModel->get_by_id($id);
        $this->data['ebook'] = $this->EbookModel->get_by_id($id);

        if ($row) {
            $this->data['title']          = 'Ubah Data ' . $this->data['module'];
            $this->data['action']         = site_url('admin/ebook/update_action');
            $this->data['button_submit']  = 'Simpan';
            $this->data['button_reset']   = 'Reset';

            $this->data['id_ebook'] = array(
                'name'  => 'id_ebook',
                'id'    => 'id_ebook',
                'type'  => 'hidden',
            );
            $this->data['judul'] = array(
                'name'  => 'judul',
                'id'    => 'judul',
                'class' => 'form-control',
            );
            $this->data['id_jenis'] = array(
                'name'        => 'id_jenis',
                'id'          => 'id_jenis',
                'class'       => 'form-control',
                'onChange'    => 'tampilKategori()',
                'required'    => '',
            );
            $this->data['id_kategori'] = array(
                'name'        => 'id_kategori',
                'id'          => 'id_kategori',
                'class'       => 'form-control',
                'required'    => '',
            );
            $this->data['sinopsis'] = array(
                'name'  => 'sinopsis',
                'id'    => 'sinopsis',
                'class' => 'form-control',
            );

            $this->data['harga_normal'] = array(
                'name'  => 'harga_normal',
                'id'    => 'b',
                'class' => 'form-control',
                'placeholder'    => 'Isikan angka saja',
                'onkeyup'            => 'hitung();',
            );

            $this->data['diskon'] = array(
                'name'  => 'diskon',
                'id'    => 'a',
                'class' => 'form-control',
                'placeholder'    => 'Isikan angka saja',
                'onkeyup'            => 'hitung();',
            );

            $this->data['harga_diskon'] = array(
                'name'  => 'harga_diskon',
                'id'    => 'd',
                'class' => 'form-control',
                'placeholder'    => 'Isikan angka saja',
                'onkeyup'            => 'hitung();',
            );

            $this->data['penulis'] = array(
                'name'  => 'penulis',
                'id'    => 'penulis',
                'class' => 'form-control',
            );

            $this->data['penerbit'] = array(
                'name'  => 'penerbit',
                'id'    => 'penerbit',
                'class' => 'form-control',
            );

            $this->data['tahun_terbit'] = array(
                'name'  => 'tahun_terbit',
                'id'    => 'tahun_terbit',
                'class' => 'form-control',
                'placeholder'    => 'Isikan tahun saja',
            );

            $this->data['stok'] = array(
                'name'        => 'stok',
                'id'          => 'stok',
                'class'       => 'form-control',
                'required'    => '',
            );

            $this->data['link_tokped'] = array(
                'name'  => 'link_tokped',
                'id'    => 'link_tokped',
                'class' => 'form-control',
            );

            $this->data['link_shopee'] = array(
                'name'  => 'link_shopee',
                'id'    => 'link_shopee',
                'class' => 'form-control',
            );

            $jenis = $row->id_jenis;
            $pilihanstok['Tersedia'] = 'Tersedia';
            $pilihanstok['Tidak Tersedia'] = 'Tidak Tersedia';
            $this->data['ambil_jenis']     = $this->JenisModel->ambil_jenis();
            $this->data['ambil_stok']     = $pilihanstok;
            $this->data['ambil_kategori']       = $this->KategoriModel->ambil_kategori($jenis);

            $this->load->view('back/ebook/ebook_edit', $this->data);
        } else {
            $this->session->set_flashdata('message', '
          <div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
            <i class="ace-icon fa fa-bullhorn green"></i> Data tidak ditemukan
          </div>');
            redirect(site_url('admin/ebook'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_ebook'));
        } else {
            $nmfile = strtolower(url_title($this->input->post('judul'))) . date('YmdHis');
            $id['id_ebook'] = $this->input->post('id_ebook');

            /* Jika file upload diisi */
            if ($_FILES['foto']['error'] <> 4) {
                $nmfile = strtolower(url_title($this->input->post('judul'))) . date('YmdHis');

                //load uploading file library
                $config['upload_path']      = './assets/images/ebook/';
                $config['allowed_types']    = 'jpg|jpeg|png|gif';
                $config['max_size']         = '2048'; // 2 MB
                $config['file_name']        = $nmfile; //nama yang terupload nantinya

                $this->load->library('upload', $config);

                // Jika file gagal diupload -> kembali ke form update
                if (!$this->upload->do_upload('foto')) {
                    //file gagal diupload -> kembali ke form update
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert">' . $error['error'] . '</div>');

                    $this->update($this->input->post('id_ebook'));
                }
                // Jika file berhasil diupload -> lanjutkan ke query INSERT
                else {
                    $delete = $this->EbookModel->del_by_id($this->input->post('id_ebook'));

                    $dir        = "assets/images/ebook/" . $delete->foto . $delete->foto_type;
                    $dir_thumb  = "assets/images/ebook/" . $delete->foto . '_thumb' . $delete->foto_type;

                    if (file_exists($dir)) {
                        // Hapus foto dan thumbnail
                        unlink($dir);
                        unlink($dir_thumb);
                    }

                    $foto = $this->upload->data();
                    // library yang disediakan codeigniter
                    $thumbnail                = $config['file_name'];
                    //nama yang terupload nantinya
                    $config['image_library']  = 'gd2';
                    // gambar yang akan dibuat thumbnail
                    $config['source_image']   = './assets/images/ebook/' . $foto['file_name'] . '';
                    // membuat thumbnail
                    $config['create_thumb']   = TRUE;
                    // rasio resolusi
                    $config['maintain_ratio'] = FALSE;
                    // lebar
                    $config['width']          = 310;
                    // tinggi
                    $config['height']         = 400;

                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $data = array(
                        'judul'  => $this->input->post('judul'),
                        'slug_ebook'   => strtolower(url_title($this->input->post('judul'))),
                        'id_jenis'          => $this->input->post('id_jenis'),
                        'id_kategori'       => $this->input->post('id_kategori'),
                        'harga_normal'       => $this->input->post('harga_normal'),
                        'diskon'       => $this->input->post('diskon'),
                        'harga_diskon'       => $this->input->post('harga_diskon'),
                        'penulis'       => $this->input->post('penulis'),
                        'penerbit'       => $this->input->post('penerbit'),
                        'tahun_terbit'       => $this->input->post('tahun_terbit'),
                        'stok'       => $this->input->post('stok'),
                        'sinopsis'    => $this->input->post('sinopsis'),
                        'link_tokped'      => $this->input->post('link_tokped'),
                        'link_shopee'      => $this->input->post('link_shopee'),
                        'foto'          => $nmfile,
                        'foto_type'     => $foto['file_ext'],
                        'update_at' => date('Y-m-d H:i:s'),
                        'update_by' => $_SESSION["id_user"]
                    );

                    $this->EbookModel->update($this->input->post('id_ebook'), $data);
                    $this->session->set_flashdata('message', '
                <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                  <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil disimpan
                </div>');
                    redirect(site_url('admin/ebook'));
                }
            }
            // Jika file upload kosong
            else {
                $data = array(
                    'judul'  => $this->input->post('judul'),
                    'slug_ebook'   => strtolower(url_title($this->input->post('judul'))),
                    'id_jenis'          => $this->input->post('id_jenis'),
                    'id_kategori'       => $this->input->post('id_kategori'),
                    'harga_normal'       => $this->input->post('harga_normal'),
                    'diskon'       => $this->input->post('diskon'),
                    'harga_diskon'       => $this->input->post('harga_diskon'),
                    'penulis'       => $this->input->post('penulis'),
                    'penerbit'       => $this->input->post('penerbit'),
                    'tahun_terbit'       => $this->input->post('tahun_terbit'),
                    'stok'       => $this->input->post('stok'),
                    'sinopsis'    => $this->input->post('sinopsis'),
                    'link_tokped'      => $this->input->post('link_tokped'),
                    'link_shopee'      => $this->input->post('link_shopee'),
                    'update_at' => date('Y-m-d H:i:s'),
                    'update_by' => $_SESSION["id_user"]
                );

                $this->EbookModel->update($this->input->post('id_ebook'), $data);
                $this->session->set_flashdata('message', '
              <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil disimpan
              </div>');
                redirect(site_url('admin/ebook'));
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->EbookModel->del_by_id($id);

        // menyimpan lokasi gambar dalam variable
        $dir = "assets/images/ebook/" . $delete->foto . $delete->foto_type;
        $dir_thumb = "assets/images/ebook/" . $delete->foto . '_thumb' . $delete->foto_type;

        // Hapus foto
        unlink($dir);
        unlink($dir_thumb);

        // Jika data ditemukan, maka hapus foto dan record nya
        if ($delete) {
            $this->EbookModel->delete($id);

            $this->session->set_flashdata('message', '
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
          <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil dihapus
        </div>');
            redirect(site_url('admin/ebook'));
        }
        // Jika data tidak ada
        else {
            $this->session->set_flashdata('message', '
          <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                      <i class="ace-icon fa fa-bullhorn green"></i> Data tidak ditemukan
          </div>');
            redirect(site_url('admin/ebook'));
        }
    }

    public function update_best()
    {
        $total_best =  $this->EbookModel->count_best_seller();
        if ($this->input->post('best_seller') == '1') {
            if ($total_best < 5) {
                $data['best_seller'] = '1';
                $this->EbookModel->update($this->input->post('id_ebook'), $data);
                $output = array(
                    "error" => false,
                    "message" => 'Success'
                );
            } else {
                $output = array(
                    "error" => true,
                    "message" => 'Maksimal ' . $total_best . ', silahkan uncheck terlebih dahulu'
                );
            }
        } else {
            $data['best_seller'] = '0';
            $this->EbookModel->update($this->input->post('id_ebook'), $data);
            $output = array(
                "error" => false,
                "message" => 'Success'
            );
        }
        echo json_encode($output);
    }


    public function _rules()
    {
        $this->form_validation->set_rules('judul', 'Judul Ebook', 'trim|required');

        // set pesan form validasi error
        $this->form_validation->set_message('required', '{field} wajib diisi');

        $this->form_validation->set_rules('id_ebook', 'id_ebook', 'trim');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert">', '</div>');
    }
}
