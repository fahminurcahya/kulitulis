<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('JenisModel');

        $this->data['module'] = 'jenis';

        if (!$this->ion_auth->is_superadmin() && !$this->ion_auth->is_admin()) {
            redirect(base_url());
        }
        // elseif($this->ion_auth->is_user()){redirect('admin/auth/login', 'refresh');}
    }

    public function index()
    {
        $this->data['title'] = 'Data ' . $this->data['module'];
        $this->load->view('back/jenis/index', $this->data);
    }

    public function ajax_list()
    {
        //get_datatables terletak di model
        $list = $this->JenisModel->get_datatables();
        $data = array();
        $no = $_POST['start'];

        // Membuat loop/ perulangan
        foreach ($list as $data_jenis) {
            $no++;
            $row = array();
            $row[] = '<p style="text-align: center">' . $no . '</p>';
            $row[] = '<p style="text-align: center">' . $data_jenis->nama_jenis . '</p>';
            // $row[] = '<p style="text-align: center">'.$data_jenis->created.'</p>';

            // Penambahan tombol edit dan hapus
            $row[] = '
      <p style="text-align: center">
      	<a class="btn btn-sm btn-warning" href="' . base_url('admin/jenis/update/') . $data_jenis->id_jenis . '" title="Edit"><i class="fa fa-pencil"></i></a>
        <a class="btn btn-sm btn-danger" href="' . base_url('admin/jenis/delete/') . $data_jenis->id_jenis . '" title="Hapus" onclick="javasciprt: return confirm(\'Apakah Anda yakin ?\')"><i class="glyphicon glyphicon-remove"></i></a>
			</p>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->JenisModel->count_all(),
            "recordsFiltered" => $this->JenisModel->count_filtered(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    public function create()
    {
        $this->data['title']          = 'Tambah Data ' . $this->data['module'];
        $this->data['action']         = site_url('admin/jenis/create_action');
        $this->data['button_submit']  = 'Simpan';
        $this->data['button_reset']   = 'Reset';

        $this->data['id_jenis'] = array(
            'name'  => 'id_jenis',
            'id'    => 'id_jenis',
            'type'  => 'hidden',
        );
        $this->data['nama_jenis'] = array(
            'name'  => 'nama_jenis',
            'id'    => 'nama_jenis',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nama_jenis'),
        );

        $this->load->view('back/jenis/jenis_add', $this->data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nama_jenis'  => $this->input->post('nama_jenis'),
                'slug_jenis'        => strtolower(url_title($this->input->post('nama_jenis'))),
            );

            // eksekusi query INSERT
            $this->JenisModel->insert($data);
            // set pesan data berhasil dibuat
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data berhasil dibuat</div>');
            redirect(site_url('admin/jenis'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_jenis', 'Nama Jenis', 'trim|required');

        // set pesan form validasi error
        $this->form_validation->set_message('required', '{field} wajib diisi');

        $this->form_validation->set_rules('id_jenis', 'id_jenis', 'trim');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert">', '</div>');
    }

    public function update($id)
    {
        $row = $this->JenisModel->get_by_id($id);
        $this->data['jenis'] = $this->JenisModel->get_by_id($id);

        if ($row) {
            $this->data['title']          = 'Ubah Data ' . $this->data['module'];
            $this->data['action']         = site_url('admin/jenis/update_action');
            $this->data['button_submit']  = 'Simpan';
            $this->data['button_reset']   = 'Reset';

            $this->data['id_jenis'] = array(
                'name'  => 'id_jenis',
                'id'    => 'id_jenis',
                'type'  => 'hidden',
            );

            $this->data['nama_jenis'] = array(
                'name'  => 'nama_jenis',
                'id'    => 'nama_jenis',
                'type'  => 'text',
                'class' => 'form-control',
            );

            $this->load->view('back/jenis/jenis_edit', $this->data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning alert">Data tidak ditemukan</div>');
            redirect(site_url('admin/jenis'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_jenis'));
        } else {
            $data = array(
                'nama_jenis'  => $this->input->post('nama_jenis'),
                'slug_jenis'        => strtolower(url_title($this->input->post('nama_jenis'))),
            );

            $this->JenisModel->update($this->input->post('id_jenis'), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Edit Data Berhasil</div>');
            redirect(site_url('admin/jenis'));
        }
    }

    public function delete($id)
    {
        $row = $this->JenisModel->get_by_id($id);

        if ($row) {
            $deleted = $this->JenisModel->delete($id);
            if ($deleted) {
                $this->session->set_flashdata('message', '
                <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                <i class="ace-icon fa fa-bullhorn green"></i> Hapus Data Berhasil
                </div>');
            } else {
                $this->session->set_flashdata('message', '
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                <i class="ace-icon fa fa-bullhorn green"></i> Hapus Data Gagal
                </div>');
            }
            redirect(site_url('admin/jenis'));
        }
        // Jika data tidak ada
        else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning alert">Data tidak ditemukan</div>');
            redirect(site_url('admin/jenis'));
        }
    }
}
