<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('ProfileModel');

        $this->data['module'] = 'profile';

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
    }

    public function update()
    {

        $id = $this->session->userdata("id_user");

        $row = $this->ProfileModel->get_by_id($id);
        $this->data['profile'] = $this->ProfileModel->get_by_id($id);

        if ($row) {
            // if($this->session->userdata("company_id") != $row->)
            // {
            $this->data['title']          = 'Update User';
            $this->data['action']         = site_url('admin/profile/update_action');
            $this->data['button_submit']  = 'Update';
            $this->data['button_reset']   = 'Reset';

            $this->data['id_user'] = array(
                'name'  => 'id_user',
                'id'    => 'id_user',
                'type'  => 'hidden',
            );

            $this->data['nama_lengkap'] = array(
                'name'  => 'nama_lengkap',
                'id'    => 'nama_lengkap',
                'class' => 'form-control',
            );

            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'class' => 'form-control',
                'disabled' => true
            );

            $this->data['nama_pena'] = array(
                'name'  => 'nama_pena',
                'id'    => 'nama_pena',
                'class' => 'form-control',
            );

            $this->data['nomor_hp'] = array(
                'name'  => 'nomor_hp',
                'id'    => 'nomor_hp',
                'class' => 'form-control',
                'type'  => 'number',
            );

            $this->data['nama_bank'] = array(
                'name'  => 'nama_bank',
                'id'    => 'nama_bank',
                'class' => 'form-control',
            );

            $this->data['nomor_rekening'] = array(
                'name'  => 'nomor_rekening',
                'id'    => 'nomor_rekening',
                'class' => 'form-control',
                'type'  => 'number',
            );

            $this->data['atas_nama'] = array(
                'name'  => 'atas_nama',
                'id'    => 'atas_nama',
                'class' => 'form-control',
            );

            $this->data['tentang_saya'] = array(
                'name'  => 'tentang_saya',
                'id'    => 'tentang_saya',
                'class' => 'form-control',
                'rows'  => '2',
            );
            $this->data['password'] = array(
                'name' => 'password',
                'id'   => 'password',
                'class'  => 'form-control',
                'placeholder'  => 'diisi jika mengubah password'
            );
            $this->data['password_confirm'] = array(
                'name' => 'password_confirm',
                'id'   => 'password_confirm',
                'class'  => 'form-control',
                'placeholder'  => 'diisi jika mengubah password'
            );

            $this->load->view('back/profile/profile_edit', $this->data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning alert">Data tidak ditemukan</div>');
            redirect(site_url('admin/profile/update'));
        }
    }

    public function update_action()
    {
        // die($this->input->post('password'));
        $this->_rules();
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_user'));
        } else {
            $id['id_user'] = $this->input->post('id_user');

            /* Jika file upload diisi */
            if ($_FILES['foto']['error'] <> 4) {
                $nmfile = strtolower(url_title($this->input->post('foto'))) . date('YmdHis');

                //load uploading file library
                $config['upload_path']      = './assets/images/user/';
                $config['allowed_types']    = 'jpg|jpeg|png|gif';
                $config['max_size']         = '2048'; // 2 MB
                $config['file_name']        = $nmfile; //nama yang terupload nantinya

                $this->load->library('upload', $config);

                // Jika file gagal diupload -> kembali ke form update
                if (!$this->upload->do_upload('foto')) {
                    //file gagal diupload -> kembali ke form update
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert">' . $error['error'] . '</div>');

                    $this->update($this->input->post('id_user'));
                }
                // Jika file berhasil diupload -> lanjutkan ke query INSERT
                else {
                    $delete = $this->ProfileModel->del_by_id($this->input->post('id_user'));

                    $dir        = "assets/images/user/" . $delete->foto . $delete->foto_type;
                    $dir_thumb  = "assets/images/user/" . $delete->foto . '_thumb' . $delete->foto_type;

                    if (file_exists($dir)) {
                        // Hapus foto dan thumbnail
                        unlink($dir);
                        unlink($dir_thumb);
                    }

                    $foto = $this->upload->data();
                    // library yang disediakan codeigniter
                    $thumbnail                = $config['file_name'];
                    //nama yang terupload nantinya
                    $config['image_library']  = 'gd2';
                    // gambar yang akan dibuat thumbnail
                    $config['source_image']   = './assets/images/user/' . $foto['file_name'] . '';
                    // membuat thumbnail
                    $config['create_thumb']   = TRUE;
                    // rasio resolusi
                    $config['maintain_ratio'] = FALSE;
                    // lebar
                    $config['width']          = 400;
                    // tinggi
                    $config['height']         = 400;

                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $data = array(
                        'nama_lengkap'      => $this->input->post('nama_lengkap'),
                        'nama_pena'     => $this->input->post('nama_pena'),
                        'nama_bank'     => $this->input->post('nama_bank'),
                        'nomor_rekening'    => $this->input->post('nomor_rekening'),
                        'atas_nama'       => $this->input->post('atas_nama'),
                        'nomor_hp'       => $this->input->post('nomor_hp'),
                        'tentang_saya'       => $this->input->post('tentang_saya'),
                        'foto'              => $nmfile,
                        'foto_type'         => $foto['file_ext'],
                        'update_at' => date('Y-m-d H:i:s')
                    );
                    if ($this->input->post('password')) {
                        $data['password'] = $this->input->post('password');
                    }
                    if ($this->ion_auth->update($this->input->post('id_user'), $data)) {
                        $this->session->set_flashdata('message', '
              <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil disimpan
              </div>');
                    } else {
                        $this->session->set_flashdata('message', '
                        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                            <i class="ace-icon fa fa-bullhorn green"></i> Update Data Gagal
                        </div>');
                    }
                    redirect(site_url('info/profile'));
                }
            }
            // Jika file upload kosong
            else {
                $data = array(
                    'nama_lengkap'      => $this->input->post('nama_lengkap'),
                    'nama_pena'     => $this->input->post('nama_pena'),
                    'nama_bank'     => $this->input->post('nama_bank'),
                    'nomor_rekening'    => $this->input->post('nomor_rekening'),
                    'atas_nama'       => $this->input->post('atas_nama'),
                    'nomor_hp'       => $this->input->post('nomor_hp'),
                    'tentang_saya'       => $this->input->post('tentang_saya'),
                    'update_at' => date('Y-m-d H:i:s')
                );

                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                if ($this->ion_auth->update($this->input->post('id_user'), $data)) {
                    $this->session->set_flashdata('message', '
              <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil disimpan
              </div>');
                } else {
                    $this->session->set_flashdata('message', '
                        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                            <i class="ace-icon fa fa-bullhorn green"></i> Update Data Gagal
                        </div>');
                }

                redirect(site_url('admin/profile/update/' . $id['id_user']));
            }
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
        $this->form_validation->set_rules('nama_pena', 'Nama Pena');
        $this->form_validation->set_rules('nomor_hp', 'No. HP', 'numeric');
        $this->form_validation->set_rules('nama_bank', 'Nama Bank');
        $this->form_validation->set_rules('nomor_rekening', 'Nomor Rekening');
        $this->form_validation->set_rules('atas_nama', 'Atas Nama');
        $this->form_validation->set_rules('tentang_saya', 'Tentang Saya');

        // set pesan form validasi error
        $this->form_validation->set_message('required', '{field} wajib diisi');
        $this->form_validation->set_message('valid_email', '{field} wajib diisi');
        $this->form_validation->set_message('numeric', '{field} diisi angka saja');

        $this->form_validation->set_rules('id_user', 'id_user', 'trim');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert" align="left">', '</div>');
    }
}
