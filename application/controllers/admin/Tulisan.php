<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tulisan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('TulisanModel');
        $this->load->model('JenisModel');
        $this->load->model('KategoriModel');
        $this->load->model('UsersModel');



        $this->data['module'] = 'Tulisan';

        if (!$this->ion_auth->is_admin()) {
            redirect(base_url());
        }
        // elseif($this->ion_auth->is_user()){redirect('admin/auth/login', 'refresh');}
    }

    public function list($status)
    {
        if ($status == 'baru-perbaikan') {
            $this->data['title'] = "List Tulisan Baru dan Perbaikan";
        } else if ($status == 'approve') {
            $this->data['title'] = "List Tulisan Disetujui";
        } else if ($status == 'paid') {
            $this->data['title'] = "List Tulisan Terbayar";
        }
        $this->load->view('back/tulisan/' . $status, $this->data);
    }

    public function ajax_list($status)
    {
        $flag = 'N';
        //get_datatables terletak di model
        if ($status == 'baru-perbaikan') {
            $status = 'S';
        } else if ($status == 'approve') {
            $status = 'A';
        } else if ($status == 'paid') {
            $status = 'A';
            $flag = 'Y';
        }

        $list = $this->TulisanModel->get_datatables($status, $flag);
        $data = array();
        $no = $_POST['start'];

        // Membuat loop/ perulangan
        foreach ($list as $data_tulisan) {

            $no++;
            $row = array();
            $row[] = '<p style="text-align: center">' . $no . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->judul . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_jenis . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_kategori . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_lengkap . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_pena . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->tgl_kirim . '</p>';
            if ($status == 'A' && $flag == 'N') {
                $row[] = '<p style="text-align: left">' . $data_tulisan->tgl_disetujui . '</p>';
                $row[] = '
                <p style="text-align: center">
                  <a class="btn btn-sm btn-info" href="#" title="Lihat" onClick="bayar(' . $data_tulisan->id_tulisan . ')"><i class="fa fa-money"> Bayar</i></a>
                </p>';
            } else if ($status == 'A' && $flag == 'Y') {
                $row[] = '<p style="text-align: left">' . $data_tulisan->tgl_disetujui . '</p>';
                $row[] = '<p style="text-align: left">' . $data_tulisan->tgl_bayar . '</p>';
            } else {
                $row[] = '
                <p style="text-align: center">
                  <a class="btn btn-sm btn-info" href="' . base_url('admin/tulisan/detail/') . $data_tulisan->id_tulisan . '" title="Lihat"><i class="fa fa-eye"> Lihat</i></a>
                </p>';
            }
            // Penambahan tombol edit dan hapus


            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->TulisanModel->count_all(),
            "recordsFiltered" => $this->TulisanModel->count_filtered($status, $flag),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    public function detail($id)
    {
        $this->data['title'] = "Detail Tulisan";
        $this->data['data_tulisan'] = $this->TulisanModel->get_by_id($id);
        $this->data['button_approve']  = 'Setujui';
        $this->data['button_reject']   = 'Tolak';
        $id_user = $_SESSION['id_user'];
        $row = $this->TulisanModel->get_by_id($id);

        if ($row) {

            $this->data['id_tulisan'] = array(
                'name'  => 'id_tulisan',
                'id'    => 'id_tulisan',
                'type'  => 'hidden',
                'value' => $id,

            );

            $this->data['judul'] = array(
                'name'  => 'judul',
                'id'    => 'judul',
                'class' => 'form-control',
                'disabled' => 'disabled',

            );

            $this->data['id_jenis'] = array(
                'name'        => 'id_jenis',
                'id'          => 'id_jenis',
                'class'       => 'form-control',
                'onChange'    => 'tampilKategori()',
                'required'    => '',
                'disabled' => 'disabled',

            );
            $this->data['id_kategori'] = array(
                'name'        => 'id_kategori',
                'id'          => 'id_kategori',
                'class'       => 'form-control',
                'required'    => '',
                'disabled' => 'disabled',

            );

            $this->data['sinopsis'] = array(
                'name'  => 'sinopsis',
                'id'    => 'sinopsis',
                'class' => 'form-control',
                'rows'  => '2',
                'disabled' => 'disabled',

            );

            $this->data['tulisan'] = array(
                'name'  => 'tulisan',
                'id'    => 'tulisan',
                'class' => 'form-control',
                'disabled' => 'disabled',

            );

            $this->data['nama_pena'] = array(
                'name'  => 'nama_pena',
                'id'    => 'nama_pena',
                'class' => 'form-control',
                'disabled' => 'disabled',
            );

            $this->data['ambil_jenis'] = $this->JenisModel->ambil_jenis();
            $jenis = $row->id_jenis;
            $this->data['ambil_kategori']       = $this->KategoriModel->ambil_kategori($jenis);
            $this->load->view('back/tulisan/detail', $this->data);
        } else {
            $this->session->set_flashdata('message', '
          <div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
            <i class="ace-icon fa fa-bullhorn green"></i> Data tidak ditemukan
          </div>');
            redirect(site_url('tulisan/list/baru-perbaikan'));
        }
    }

    public function update_action($status)
    {
        if ($status == 'approve') {
            $status = 'A';
            $data['tgl_disetujui'] = date('Y-m-d');
            $id_tulisan = $this->input->post('id_tulisan');
        } else if ($status == 'reject') {
            $status = 'R';
            $data['note'] = $this->input->post('note');
            $id_tulisan = $this->input->post('idTulisan');
        } else if ($status == 'paid') {
            $status = 'A';
            $id_tulisan = $this->input->post('id_tulisan');
            $data['tgl_bayar'] = date('Y-m-d');
            $data['flag_bayar'] = 'Y';
        } else {
            redirect(base_url());
        }

        $data['status'] = $status;
        $this->TulisanModel->update($id_tulisan, $data);
        if ($status == 'A') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Tulisan Disetujui</div>');
            redirect(site_url('admin/tulisan/list/approve'));
        } else if ($status == 'R') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Tulisan Ditolak</div>');
            redirect(site_url('admin/tulisan/list/baru-perbaikan'));
        } else if ($status == 'P') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Tulisan Telah Dibayar</div>');
            redirect(site_url('admin/tulisan/list/paid'));
        }
    }

    public function informasi_user($id)
    {
        $this->data['penulis'] = $this->TulisanModel->get_by_id($id);
        $row = $this->TulisanModel->get_by_id($id);

        if ($row) {
            $this->data['id_tulisan'] = array(
                'name'  => 'id_tulisan',
                'id'    => 'id_tulisan',
                'class' => 'form-control',
                'name' => 'id_tulisan',
                'type' => 'hidden'
            );

            $this->data['nama_lengkap'] = array(
                'name'  => 'nama_lengkap',
                'id'    => 'nama_lengkap',
                'class' => 'form-control',
                'disabled' => 'disabled',
            );

            $this->data['nomor_hp'] = array(
                'name'  => 'nomor_hp',
                'id'    => 'nomor_hp',
                'class' => 'form-control',
                'disabled' => 'disabled',
            );

            $this->data['nama_bank'] = array(
                'name'  => 'nama_bank',
                'id'    => 'nama_bank',
                'class' => 'form-control',
                'disabled' => 'disabled',
            );

            $this->data['nomor_rekening'] = array(
                'name'  => 'nomor_rekening',
                'id'    => 'nomor_rekening',
                'class' => 'form-control',
                'disabled' => 'disabled',
            );

            $this->data['atas_nama'] = array(
                'name'  => 'atas_nama',
                'id'    => 'atas_nama',
                'class' => 'form-control',
                'disabled' => 'disabled',
            );

            $this->load->view('back/tulisan/v_bayar', $this->data);
        } else {
            $this->session->set_flashdata('message', '
          <div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
            <i class="ace-icon fa fa-bullhorn green"></i> Data tidak ditemukan
          </div>');
            redirect(site_url('tulisan/list/baru-perbaikan'));
        }
    }
}
