<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('JenisModel');
        $this->load->model('KategoriModel');

        $this->data['module'] = 'Kategori';

        if (!$this->ion_auth->is_superadmin() && !$this->ion_auth->is_admin()) {
            redirect(base_url());
        }
        // elseif($this->ion_auth->is_user()){redirect('admin/auth/login', 'refresh');}
    }

    public function index()
    {
        $this->data['title'] = 'Data ' . $this->data['module'];
        $this->load->view('back/kategori/index', $this->data);
    }

    public function ajax_list()
    {
        //get_datatables terletak di model
        $list = $this->KategoriModel->get_datatables();
        $data = array();
        $no = $_POST['start'];

        // Membuat loop/ perulangan
        foreach ($list as $data_kategori) {
            $no++;
            $row = array();
            $row[] = '<p style="text-align: center">' . $no . '</p>';
            $row[] = '<p style="text-align: center">' . $data_kategori->nama_kategori . '</p>';
            $row[] = '<p style="text-align: center">' . $data_kategori->nama_jenis . '</p>';

            // Penambahan tombol edit dan hapus
            $row[] = '
        <p style="text-align: center">
            <a class="btn btn-sm btn-warning" href="' . base_url('admin/kategori/update/') . $data_kategori->id_kategori . '" title="Edit"><i class="fa fa-pencil"></i></a>
          <a class="btn btn-sm btn-danger" href="' . base_url('admin/kategori/delete/') . $data_kategori->id_kategori . '" title="Hapus" onclick="javasciprt: return confirm(\'Apakah Anda yakin ?\')"><i class="glyphicon glyphicon-remove"></i></a>
              </p>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->KategoriModel->count_all(),
            "recordsFiltered" => $this->KategoriModel->count_filtered(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    public function create()
    {
        $this->data['title']          = 'Tambah Data ' . $this->data['module'];
        $this->data['action']         = site_url('admin/kategori/create_action');
        $this->data['button_submit']  = 'Simpan';
        $this->data['button_reset']   = 'Reset';

        $this->data['id_jenis'] = array(
            'name'  => 'id_jenis',
            'id'    => 'id_jenis',
            'class' => 'form-control',
            'required'    => '',
        );

        $this->data['nama_kategori'] = array(
            'name'  => 'nama_kategori',
            'id'    => 'nama_kategori',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nama_kategori'),
        );

        $this->data['ambil_jenis'] = $this->JenisModel->ambil_jenis();

        $this->load->view('back/kategori/kategori_add', $this->data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_jenis'  => $this->input->post('id_jenis'),
                'nama_kategori'  => $this->input->post('nama_kategori'),
                'slug_kategori'  => strtolower(url_title($this->input->post('nama_kategori'))),
            );

            // eksekusi query INSERT
            $this->KategoriModel->insert($data);
            // set pesan data berhasil dibuat
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Data berhasil dibuat</div>');
            redirect(site_url('admin/kategori'));
        }
    }

    public function update($id)
    {
        $row = $this->KategoriModel->get_by_id($id);
        $this->data['kategori'] = $this->KategoriModel->get_by_id($id);

        if ($row) {
            $this->data['title']          = 'Ubah Data ' . $this->data['module'];
            $this->data['action']         = site_url('admin/kategori/update_action');
            $this->data['button_submit']  = 'Simpan';
            $this->data['button_reset']   = 'Reset';

            $this->data['id_kategori'] = array(
                'name'  => 'id_kategori',
                'id'    => 'id_kategori',
                'type'  => 'hidden',
            );

            $this->data['id_jenis'] = array(
                'name'  => 'id_jenis',
                'id'    => 'id_jenis',
                'class' => 'form-control',
                'required'    => '',
            );

            $this->data['nama_kategori'] = array(
                'name'  => 'nama_kategori',
                'id'    => 'nama_kategori',
                'type'  => 'text',
                'class' => 'form-control',
            );

            $this->data['ambil_jenis'] = $this->JenisModel->ambil_jenis();

            $this->load->view('back/kategori/kategori_edit', $this->data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning alert">Data tidak ditemukan</div>');
            redirect(site_url('admin/kategori'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_kategori'));
        } else {
            $data = array(
                'id_jenis'  => $this->input->post('id_jenis'),
                'nama_kategori'  => $this->input->post('nama_kategori'),
                'slug_kategori'  => strtolower(url_title($this->input->post('nama_kategori'))),
            );

            $this->KategoriModel->update($this->input->post('id_kategori'), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert">Edit Data Berhasil</div>');
            redirect(site_url('admin/kategori'));
        }
    }

    public function delete($id)
    {
        $row = $this->KategoriModel->get_by_id($id);

        if ($row) {
            $deleted = $this->KategoriModel->delete($id);
            if ($deleted) {
                $this->session->set_flashdata('message', '
                <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                <i class="ace-icon fa fa-bullhorn green"></i> Hapus Data Berhasil
                </div>');
            } else {
                $this->session->set_flashdata('message', '
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                <i class="ace-icon fa fa-bullhorn green"></i> Hapus Data Gagal
                </div>');
            }
            redirect(site_url('admin/kategori'));
        }
        // Jika data tidak ada
        else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning alert">Data tidak ditemukan</div>');
            redirect(site_url('admin/kategori'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_kategori', 'Nama kategori', 'trim|required');

        // set pesan form validasi error
        $this->form_validation->set_message('required', '{field} wajib diisi');

        $this->form_validation->set_rules('id_kategori', 'id_kategori', 'trim');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert">', '</div>');
    }
}
