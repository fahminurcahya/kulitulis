<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('PenulisModel');
        $this->load->model('TulisanModel');
        $this->load->model('ProfileModel');
        $this->load->model('EbookModel');
        $this->load->model('EventModel');
        $this->load->model('UsersModel');


        $this->data['module'] = 'Dashboard';

        if (!$this->ion_auth->is_superadmin() && !$this->ion_auth->is_admin() && !$this->ion_auth->is_penulis()) {
            redirect(base_url());
        }
    }

    public function index()
    {
        if ($this->ion_auth->is_penulis()) {
            $this->data['jumlah_dikirim'] = $this->PenulisModel->get_data_by_id_user('S', 'N');
            $this->data['jumlah_ditolak'] = $this->PenulisModel->get_data_by_id_user('R', 'N');
            $this->data['jumlah_disetujui'] = $this->PenulisModel->get_data_by_id_user('A', 'N');
            $this->data['jumlah_dibayar'] = $this->PenulisModel->get_data_by_id_user('A', 'Y');
            $id = $this->session->userdata("id_user");
            $row = $this->ProfileModel->get_by_id($id);
            if ($row->nama_bank == '' || $row->nama_bank == '' || $row->atas_nama == '') {
                $this->session->set_flashdata('warning', '
                <div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                  <i class="ace-icon fa fa-bullhorn green"></i> Silahkan Lengkapi Profile Anda </div>');
            }
        } else if ($this->ion_auth->is_admin() || $this->ion_auth->is_superadmin()) {
            $this->data['jumlah_ebook'] = $this->EbookModel->count_all();
            $this->data['jumlah_event'] = $this->EventModel->count_all();
            $this->data['jumlah_user'] = $this->UsersModel->count_all();
        } else {
            redirect(base_url());
        }


        // return var_dump($this->data);
        $this->load->view('back/dashboard', $this->data);
    }


    public function ajax_list($status)
    {
        //get_datatables terletak di model
        if ($status == 'draft_list' || $status == 'D') {
            $status = 'D';
            $this->list = $this->PenulisModel->get_datatables_limit($status);
        } else if ($status == 'dikirim' || $status == 'S') {
            $status = 'S';
            $this->list = $this->TulisanModel->get_datatables_limit($status);
        }
        $data = array();
        $no = 0;

        // Membuat loop/ perulangan
        foreach ($this->list as $data_tulisan) {

            $no++;
            $row = array();
            $row[] = '<p style="text-align: center">' . $no . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->judul . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_jenis . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_kategori . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_lengkap . '</p>';
            $row[] = '<p style="text-align: left">' . $data_tulisan->nama_pena . '</p>';
            if ($status == 'S') {
                $row[] = '<p style="text-align: left">' . $data_tulisan->tgl_kirim . '</p>';
            }
            if ($data_tulisan->status == 'D') {
                $sta = "Draft";
            } else if ($data_tulisan->status == 'S') {
                $sta = "Dikirim";
            } else if ($data_tulisan->status == 'A') {
                $sta = "Disetujui";
            } else if ($data_tulisan->status == 'R') {
                $sta = "Ditolak";
            } else if ($data_tulisan->status == 'P') {
                $sta = "Dibayar";
            } else {
                $sta = "";
            }
            $row[] = '<p style="text-align: left">' . $sta . '</p>';

            // Penambahan tombol edit dan hapus


            $data[] = $row;
        }

        $output = array(
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }
}
