<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Event extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('EventModel');

        $this->data['module'] = 'Event';

        if (!$this->ion_auth->is_superadmin() && !$this->ion_auth->is_admin()) {
            redirect(base_url());
        }
        // elseif($this->ion_auth->is_user()){redirect('admin/auth/login', 'refresh');}
    }

    public function index()
    {
        $this->data['title'] = 'Data ' . $this->data['module'];
        $this->load->view('back/event/index', $this->data);
    }

    public function ajax_list()
    {
        //get_datatables terletak di model
        $list = $this->EventModel->get_datatables();
        $data = array();
        $no = $_POST['start'];

        // Membuat loop/ perulangan
        foreach ($list as $data_event) {
            $no++;
            $row = array();
            $row[] = '<p style="text-align: center">' . $no . '</p>';
            $row[] = '<p style="text-align: left">' . $data_event->judul_event . '</p>';
            // Penambahan tombol edit dan hapus
            $row[] = '
        <p style="text-align: center">
          <a class="btn btn-sm btn-warning" href="' . base_url('admin/event/update/') . $data_event->id_event . '" title="Edit"><i class="fa fa-pencil"></i></a>
          <a class="btn btn-sm btn-danger" href="' . base_url('admin/event/delete/') . $data_event->id_event . '" title="Hapus" onclick="javasciprt: return confirm(\'Apakah Anda yakin ?\')"><i class="fa fa-remove"></i></a>
              </p>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->EventModel->count_all(),
            "recordsFiltered" => $this->EventModel->count_filtered(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }


    public function create()
    {
        $this->data['title']          = 'Tambah Data ' . $this->data['module'];
        $this->data['action']         = site_url('admin/event/create_action');
        $this->data['button_submit']  = 'Simpan';
        $this->data['button_reset']   = 'Reset';

        $this->data['judul_event'] = array(
            'name'  => 'judul_event',
            'id'    => 'judul_event',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('judul_event'),
        );

        $this->data['desc_event'] = array(
            'name'  => 'desc_event',
            'id'    => 'desc_event',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('desc_event'),
        );

        $this->load->view('back/event/event_add', $this->data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            /* 4 adalah menyatakan tidak ada file yang diupload*/
            if ($_FILES['foto']['error'] <> 4) {
                $nmfile = strtolower(url_title($this->input->post('judul_event'))) . date('YmdHis');

                /* memanggil library upload ci */
                $config['upload_path']      = './assets/images/event/';
                $config['allowed_types']    = 'jpg|jpeg|png|gif';
                $config['max_size']         = '2048'; // 2 MB
                $config['file_name']        = $nmfile; //nama yang terupload nantinya

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('foto')) {
                    //file gagal diupload -> kembali ke form tambah
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert">' . $error['error'] . '</div>');

                    $this->create();
                }
                //file berhasil diupload -> lanjutkan ke query INSERT
                else {
                    $foto = $this->upload->data();
                    $thumbnail                = $config['file_name'];
                    // library yang disediakan codeigniter
                    $config['image_library']  = 'gd2';
                    // gambar yang akan disimpan thumbnail
                    $config['source_image']   = './assets/images/event/' . $foto['file_name'] . '';
                    // membuat thumbnail
                    $config['create_thumb']   = TRUE;
                    // rasio resolusi
                    $config['maintain_ratio'] = FALSE;
                    // lebar
                    $config['width']          = 800;
                    // tinggi
                    $config['height']         = 400;

                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $data = array(
                        'judul_event'    => $this->input->post('judul_event'),
                        'slug_event'     => strtolower(url_title($this->input->post('judul_event'))),
                        'desc_event'      => $this->input->post('desc_event'),
                        'foto'          => $nmfile,
                        'foto_type'     => $foto['file_ext'],
                    );

                    // eksekusi query INSERT
                    $this->EventModel->insert($data);
                    // set pesan data berhasil disimpan
                    $this->session->set_flashdata('message', '
            <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
              <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil disimpan
            </div>');
                    redirect(site_url('admin/event'));
                }
            } else // Jika file upload kosong
            {
                $data = array(
                    'judul_event'  => $this->input->post('judul_event'),
                    'slug_event'     => strtolower(url_title($this->input->post('judul_event'))),
                    'desc_event'      => $this->input->post('desc_event'),
                );

                // eksekusi query INSERT
                $this->EventModel->insert($data);
                // set pesan data berhasil disimpan
                $this->session->set_flashdata('message', '
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
          <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil disimpan
        </div>');
                redirect(site_url('admin/event'));
            }
        }
    }

    public function update($id)
    {
        $row = $this->EventModel->get_by_id($id);
        $this->data['event'] = $this->EventModel->get_by_id($id);

        if ($row) {
            $this->data['title']          = 'Ubah Data ' . $this->data['module'];
            $this->data['action']         = site_url('admin/event/update_action');
            $this->data['button_submit']  = 'Simpan';
            $this->data['button_reset']   = 'Reset';

            $this->data['id_event'] = array(
                'name'  => 'id_event',
                'id'    => 'id_event',
                'type'  => 'hidden',
            );
            $this->data['judul_event'] = array(
                'name'  => 'judul_event',
                'id'    => 'judul_event',
                'class' => 'form-control',
            );
            $this->data['desc_event'] = array(
                'name'  => 'desc_event',
                'id'    => 'desc_event',
                'class' => 'form-control',
            );

            $this->load->view('back/event/event_edit', $this->data);
        } else {
            $this->session->set_flashdata('message', '
          <div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
            <i class="ace-icon fa fa-bullhorn green"></i> Data tidak ditemukan
          </div>');
            redirect(site_url('admin/event'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_event'));
        } else {
            $nmfile = strtolower(url_title($this->input->post('judul_event'))) . date('YmdHis');
            $id['id_event'] = $this->input->post('id_event');

            /* Jika file upload diisi */
            if ($_FILES['foto']['error'] <> 4) {
                $nmfile = strtolower(url_title($this->input->post('judul_event'))) . date('YmdHis');

                //load uploading file library
                $config['upload_path']      = './assets/images/event/';
                $config['allowed_types']    = 'jpg|jpeg|png|gif';
                $config['max_size']         = '2048'; // 2 MB
                $config['file_name']        = $nmfile; //nama yang terupload nantinya

                $this->load->library('upload', $config);

                // Jika file gagal diupload -> kembali ke form update
                if (!$this->upload->do_upload('foto')) {
                    //file gagal diupload -> kembali ke form update
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert">' . $error['error'] . '</div>');

                    $this->update($this->input->post('id_event'));
                }
                // Jika file berhasil diupload -> lanjutkan ke query INSERT
                else {
                    $delete = $this->EventModel->del_by_id($this->input->post('id_event'));

                    $dir        = "assets/images/event/" . $delete->foto . $delete->foto_type;
                    $dir_thumb  = "assets/images/event/" . $delete->foto . '_thumb' . $delete->foto_type;

                    if (file_exists($dir)) {
                        // Hapus foto dan thumbnail
                        unlink($dir);
                        unlink($dir_thumb);
                    }

                    $foto = $this->upload->data();
                    // library yang disediakan codeigniter
                    $thumbnail                = $config['file_name'];
                    //nama yang terupload nantinya
                    $config['image_library']  = 'gd2';
                    // gambar yang akan dibuat thumbnail
                    $config['source_image']   = './assets/images/event/' . $foto['file_name'] . '';
                    // membuat thumbnail
                    $config['create_thumb']   = TRUE;
                    // rasio resolusi
                    $config['maintain_ratio'] = FALSE;
                    // lebar
                    $config['width']          = 800;
                    // tinggi
                    $config['height']         = 400;

                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $data = array(
                        'judul_event'  => $this->input->post('judul_event'),
                        'slug_event'   => strtolower(url_title($this->input->post('judul_event'))),
                        'desc_event'    => $this->input->post('desc_event'),
                        'foto'        => $nmfile,
                        'foto_type'   => $foto['file_ext']
                    );

                    $this->EventModel->update($this->input->post('id_event'), $data);
                    $this->session->set_flashdata('message', '
                <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                  <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil disimpan
                </div>');
                    redirect(site_url('admin/event'));
                }
            }
            // Jika file upload kosong
            else {
                $data = array(
                    'judul_event'  => $this->input->post('judul_event'),
                    'slug_event'   => strtolower(url_title($this->input->post('judul_event'))),
                    'desc_event'    => $this->input->post('desc_event')
                );

                $this->EventModel->update($this->input->post('id_event'), $data);
                $this->session->set_flashdata('message', '
              <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil disimpan
              </div>');
                redirect(site_url('admin/event'));
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->EventModel->del_by_id($id);

        // menyimpan lokasi gambar dalam variable
        $dir = "assets/images/event/" . $delete->foto . $delete->foto_type;
        $dir_thumb = "assets/images/event/" . $delete->foto . '_thumb' . $delete->foto_type;

        // Hapus foto
        unlink($dir);
        unlink($dir_thumb);

        // Jika data ditemukan, maka hapus foto dan record nya
        if ($delete) {
            $this->EventModel->delete($id);

            $this->session->set_flashdata('message', '
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
          <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil dihapus
        </div>');
            redirect(site_url('admin/event'));
        }
        // Jika data tidak ada
        else {
            $this->session->set_flashdata('message', '
          <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                      <i class="ace-icon fa fa-bullhorn green"></i> Data tidak ditemukan
          </div>');
            redirect(site_url('admin/event'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('judul_event', 'Judul Event', 'trim|required');

        // set pesan form validasi error
        $this->form_validation->set_message('required', '{field} wajib diisi');

        $this->form_validation->set_rules('id_event', 'id_event', 'trim');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert">', '</div>');
    }
}
