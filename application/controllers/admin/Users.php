<?php defined('BASEPATH') or exit('No direct script access allowed');


class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('auth');
        $this->load->model('UsersModel');
        $this->load->model('Ion_auth_model');
        $this->data['module'] = 'User';
        if (!$this->ion_auth->is_superadmin()) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $this->data['title'] = 'Data ' . $this->data['module'];

        // Set pesan flash data error jika ada
        $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
        $this->data['users']     = $this->UsersModel->get_all_users()->result();
        $this->_render_page('back/users/index', $this->data);
    }

    // function untuk menampilkan form dan menambah user
    public function create_user()
    {
        $this->data['title'] = 'Tambah Data ' . $this->data['module'];

        /* setting bawaan ionauth */
        $tables                     = $this->config->item('tables', 'ion_auth');
        $identity_column     = $this->config->item('identity', 'ion_auth');

        $this->data['identity_column'] = $identity_column;

        // validasi form input
        $this->form_validation->set_rules('email', 'Email', 'valid_email|required|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('nama_pena', 'Nama Pena', 'required');
        $this->form_validation->set_rules('nomor_hp', 'No. Telp', 'numeric|trim');
        if ($this->input->post('id_level') != 1 && $this->input->post('id_level') != 2) {
            $this->form_validation->set_rules('nama_bank', 'Nama Bank', 'required');
            $this->form_validation->set_rules('nomor_rekening', 'Nama Rekening', 'required');
            $this->form_validation->set_rules('atas_nama', 'Nama Nama', 'required');
        }

        // set_message / set pesan
        $this->form_validation->set_message('required', '{field} mohon diisi');
        $this->form_validation->set_message('valid_email', 'Format email tidak benar');
        $this->form_validation->set_message('numeric', 'No. HP harus angka');
        $this->form_validation->set_message('matches', 'Password baru dan konfirmasi harus sama');
        $this->form_validation->set_message('is_unique', '%s telah terpakai, ganti dengan yang lain');

        // /* jalan form validasi */
        if ($this->form_validation->run() == true) {
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = array(
                'email'         => strtolower($this->input->post('email')),
                'nama_lengkap'                 => $this->input->post('nama_lengkap'),
                'nama_pena'      => $this->input->post('nama_pena'),
                'nomor_hp'         => $this->input->post('nomor_hp'),
                'nama_bank'         => $this->input->post('nama_bank'),
                'nomor_rekening'         => $this->input->post('nomor_rekening'),
                'atas_nama'         => $this->input->post('atas_nama'),
                'tentang_saya'         => $this->input->post('tentang_saya'),
                'id_level'      => $this->input->post('id_level'),
                'created_by'    => $this->session->userdata('user_id')
            );

            $this->ion_auth->register($identity, $password, $email, $additional_data);

            // check to see if we are creating the user | redirect them back to the admin page
            $this->session->set_flashdata('message', '
           <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                      <i class="ace-icon fa fa-bullhorn green"></i> Data berhasil dibuat
           </div>');
            redirect('admin/users', 'refresh');
            // }
        }

        // display the create user form | set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->data['email'] = array(
            'name'  => 'email',
            'id'    => 'email',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('email'),
        );
        $this->data['password'] = array(
            'name'  => 'password',
            'id'    => 'password',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('password'),
        );
        $this->data['password_confirm'] = array(
            'name'  => 'password_confirm',
            'id'    => 'password_confirm',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('password_confirm'),
        );
        $this->data['nama_lengkap'] = array(
            'name'  => 'nama_lengkap',
            'id'    => 'nama_lengkap',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nama_lengkap'),
        );
        $this->data['nama_pena'] = array(
            'name'  => 'nama_pena',
            'id'    => 'nama_pena',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nama_pena'),
        );
        $this->data['nomor_hp'] = array(
            'name'  => 'nomor_hp',
            'id'    => 'nomor_hp',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nomor_hp'),
        );

        $this->data['usertype_css'] = array(
            'name'  => 'id_level',
            'id'    => 'id_level',
            'class' => 'form-control',
        );

        $this->data['nama_bank'] = array(
            'name'  => 'nama_bank',
            'id'    => 'nama_bank',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nama_bank'),
        );
        $this->data['nomor_rekening'] = array(
            'name'  => 'nomor_rekening',
            'id'    => 'nomor_rekening',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nomor_rekening'),
        );
        $this->data['atas_nama'] = array(
            'name'  => 'atas_nama',
            'id'    => 'atas_nama',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('atas_nama'),
        );

        $this->data['get_all_users_group']     = $this->UsersModel->get_all_users_levels();
        $this->load->view('back/users/create_user', $this->data);
    }

    // function untuk menampilkan form edit dan meng update user
    public function edit_user($id)
    {
        $this->data['title'] = 'Edit Data ' . $this->data['module'];

        // Cek hak akses ubah password user lain (Hanya Superadmin yang dibolehkan)
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_superadmin() && !($this->ion_auth->user()->row()->id_user == $id))) {
            redirect('dashboard', 'refresh');
        }

        $user = $this->ion_auth->user($id)->row();

        if ($user == FALSE) {
            $this->session->set_flashdata('message', '
                 <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                 <i class="ace-icon fa fa-bullhorn green"></i> Data tidak ditemukan
                 </div>');
            redirect('admin/auth/', 'refresh');
        }


        // validate form input
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('nama_pena', 'Nama Pena', 'required');
        $this->form_validation->set_rules('nomor_hp', 'No. Telp', 'numeric|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        if ($this->input->post('id_level') != 1 && $this->input->post('id_level') != 2) {
            $this->form_validation->set_rules('nama_bank', 'Nama Bank', 'required');
            $this->form_validation->set_rules('nomor_rekening', 'Nama Rekening', 'required');
            $this->form_validation->set_rules('atas_nama', 'Nama Nama', 'required');
        }

        // set pesan
        $this->form_validation->set_message('required', '{field} mohon diisi');
        $this->form_validation->set_message('numeric', 'No. HP harus angka');
        $this->form_validation->set_message('valid_email', 'Format email salah');
        $this->form_validation->set_message('min_length', 'Password minimal 8 huruf');
        $this->form_validation->set_message('max_length', 'Password maksimal 20 huruf');
        $this->form_validation->set_message('matches', 'Password baru dan konfirmasi harus sama');

        if (isset($_POST) && !empty($_POST)) {
            // mengecek validitas request update data
            if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {
                show_error($this->lang->line('error_csrf'));
            }

            // update password jika dimasukkan/ diisi
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() === TRUE) {
                /* jika ada file foto yang ingin diupload*/
                $data = array(
                    'email'         => strtolower($this->input->post('email')),
                    'nama_lengkap'                 => $this->input->post('nama_lengkap'),
                    'nama_pena'      => $this->input->post('nama_pena'),
                    'nomor_hp'         => $this->input->post('nomor_hp'),
                    'nama_bank'         => $this->input->post('nama_bank'),
                    'nomor_rekening'         => $this->input->post('nomor_rekening'),
                    'atas_nama'         => $this->input->post('atas_nama'),
                    'tentang_saya'         => $this->input->post('tentang_saya'),
                    'id_level'      => $this->input->post('id_level'),
                    'created_by'    => $this->session->userdata('user_id')
                );

                // jika password terisi
                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                // mengecek apakah sedang mengupdate data user
                if ($this->ion_auth->update($user->id_user, $data)) {
                    $this->session->set_flashdata('message', '
                                 <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                                     <i class="ace-icon fa fa-bullhorn green"></i> Update Data Berhasil
                                 </div>');
                    if (!$this->ion_auth->is_superadmin()) {
                        redirect(site_url('admin/users/edit_user/' . $this->session->user_id));
                    } else {
                        redirect(site_url('admin/users/'));
                    }
                } else {
                    $this->session->set_flashdata('message', '
                                 <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                                     <i class="ace-icon fa fa-bullhorn green"></i> Update Data Gagal
                                 </div>');
                    if (!$this->ion_auth->is_superadmin()) {
                        redirect(site_url('admin/users/edit_user/' . $this->session->user_id));
                    } else {
                        redirect(site_url('admin/users/'));
                    }
                }
            }
        }

        // menampilkan form edit/ update data user
        $this->data['csrf'] = $this->_get_csrf_nonce();

        // mengatur pesan/ flash data eror jika ada
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // melempar data user ke view
        $this->data['user'] = $user;
        $this->data['email'] = array(
            'name'  => 'email',
            'id'    => 'email',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('email', $user->email),
        );

        $this->data['nama_lengkap'] = array(
            'name'  => 'nama_lengkap',
            'id'    => 'nama_lengkap',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nama_lengkap', $user->nama_lengkap),
        );
        $this->data['nama_pena'] = array(
            'name'  => 'nama_pena',
            'id'    => 'nama_pena',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nama_pena', $user->nama_pena),
        );
        $this->data['nomor_hp'] = array(
            'name'  => 'nomor_hp',
            'id'    => 'nomor_hp',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nomor_hp', $user->nomor_hp),
        );

        $this->data['id_level'] = array(
            'name'  => 'id_level',
            'id'    => 'id_level',
            'class' => 'form-control',
        );

        $this->data['nama_bank'] = array(
            'name'  => 'nama_bank',
            'id'    => 'nama_bank',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nama_bank', $user->nama_bank),
        );
        $this->data['nomor_rekening'] = array(
            'name'  => 'nomor_rekening',
            'id'    => 'nomor_rekening',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nomor_rekening', $user->nomor_rekening),
        );
        $this->data['atas_nama'] = array(
            'name'  => 'atas_nama',
            'id'    => 'atas_nama',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('atas_nama', $user->atas_nama),
        );
        $this->data['password'] = array(
            'name' => 'password',
            'id'   => 'password',
            'class'  => 'form-control',
            'placeholder'  => 'diisi jika mengubah password'
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id'   => 'password_confirm',
            'class'  => 'form-control',
            'placeholder'  => 'diisi jika mengubah password'
        );

        $this->data['get_all_users_group'] = $this->UsersModel->get_all_users_levels();
        $this->_render_page('back/users/edit_user', $this->data);
    }

    public function delete_user($id)
    {
        $deleted = $this->Ion_auth_model->delete_user($id);
        if ($deleted) {
            $this->session->set_flashdata('message', '
            <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
            <i class="ace-icon fa fa-bullhorn green"></i> Hapus Data Berhasil
            </div>');
        } else {
            $this->session->set_flashdata('message', '
            <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
            <i class="ace-icon fa fa-bullhorn green"></i> Hapus Data Gagal
            </div>');
        }

        redirect(site_url('admin/users/'));
    }


    // function untuk meng aktifkan user
    public function activate($id, $code = FALSE)
    {
        if ($code !== false) {
            $activation = $this->ion_auth->activate($id, $code);
        } else if ($this->ion_auth->is_superadmin()) {
            $activation = $this->ion_auth->activate($id);
        }

        if ($activation) {
            // redirect them to the auth page
            $this->session->set_flashdata('message', '
             <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                 <i class="ace-icon fa fa-bullhorn green"></i> Akun berhasil diaktifkan
             </div>');
            redirect("admin/auth/", 'refresh');
        } else {
            // redirect them to the forgot password page
            $this->session->set_flashdata('message', '
             <div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
                 <i class="ace-icon fa fa-bullhorn green"></i> Akun gagal diaktifkan
             </div>');
            redirect("admin/auth/", 'refresh');
        }
    }

    // fungction untuk deactivekan user
    public function deactivate($id = NULL)
    {
        $id = (int) $id;

        // mengecek usertype user apakah super bukan
        if ($this->ion_auth->logged_in() && $this->ion_auth->is_superadmin()) {
            $this->ion_auth->deactivate($id);
        }

        // mengarahkan ke halaman user/ data user
        $this->session->set_flashdata('message', '
         <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
             <i class="ace-icon fa fa-bullhorn green"></i> Akun berhasil dinonaktifkan
         </div>');
        redirect('admin/auth/', 'refresh');
    }


    // generate csrf bawaan librari
    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public function _valid_csrf_nonce()
    {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


    public function _render_page($view, $data = NULL, $returnhtml = FALSE) //I think this makes more sense
    {
        $this->viewdata = (empty($data)) ? $this->data : $data;
        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        // This will return html on 3rd argument being true
        if ($returnhtml) {
            return $view_html;
        }
    }
}
