<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');

        $this->load->model('CompanyModel');
        $this->load->model('Ion_auth_model');
        $this->data['module'] = 'Auth';
    }

    public function register()
    {
        $this->data['title']                             = 'Register/ Login';

        /* setting bawaan ionauth */
        $tables                     = $this->config->item('tables', 'ion_auth');
        $identity_column     = $this->config->item('identity', 'ion_auth');

        $this->data['identity_column'] = $identity_column;
        $this->data['company_data']             = $this->CompanyModel->get_by_company();


        // validasi form input
        $this->form_validation->set_rules('email', 'Email', 'valid_email|required|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('nama_pena', 'Nama Pena', 'required');
        $this->form_validation->set_rules('nomor_hp', 'No. Telp', 'numeric|trim');

        // set_message / set pesan
        $this->form_validation->set_message('required', '{field} mohon diisi');
        $this->form_validation->set_message('valid_email', 'Format email tidak benar');
        $this->form_validation->set_message('numeric', 'No. HP harus angka');
        $this->form_validation->set_message('matches', 'Password baru dan konfirmasi harus sama');
        $this->form_validation->set_message('is_unique', '%s telah terpakai, ganti dengan yang lain');

        // set pesan
        $this->form_validation->set_message('required', '{field} mohon diisi');
        $this->form_validation->set_message('valid_email', 'Format email tidak benar');
        $this->form_validation->set_message('numeric', 'No. HP harus angka');
        $this->form_validation->set_message('matches', 'Password baru dan konfirmasi harus sama');
        $this->form_validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');

        // cek form_validation dan register ke db
        if ($this->form_validation->run() == true) {
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            // data tambahan yang untuk dimasukkan pada tabel
            $additional_data = array(
                'email'         => strtolower($this->input->post('email')),
                'nama_lengkap'                 => $this->input->post('nama_lengkap'),
                'nama_pena'      => $this->input->post('nama_pena'),
                'nomor_hp'         => $this->input->post('nomor_hp'),
                'id_level'      => 3
            );

            // mengirimkan data yang sudah disediakan diatas $additional_data $email, $identity $password
            $this->ion_auth->register($identity, $password, $email, $additional_data);

            // check to see if we are creating the user | redirect them back to the admin page
            $this->session->set_flashdata('message', '
			<div class="col-lg-12">
				<div class="alert alert-success alert">Registrasi Berhasil, silahkan login untuk mulai menulis.</div>
			</div>');
            redirect(base_url());
        } else {
            // display the create user form | set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('password_confirm'),
            );
            $this->data['nama_lengkap'] = array(
                'name'  => 'nama_lengkap',
                'id'    => 'nama_lengkap',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('nama_lengkap'),
            );
            $this->data['nama_pena'] = array(
                'name'  => 'nama_pena',
                'id'    => 'nama_pena',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('nama_pena'),
            );
            $this->data['nomor_hp'] = array(
                'name'  => 'nomor_hp',
                'id'    => 'nomor_hp',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('nomor_hp'),
            );
            $this->load->view('front/auth/register', $this->data);
        }
    }



    public function login()
    {
        $this->data['title']                             = 'Login';

        // cek sudah login/belum
        if ($this->ion_auth->logged_in()) {
            redirect(base_url());
        }

        // panggil library recaptcha
        // $this->load->library('Recaptcha');

        // siapkan data recaptcha
        // $this->data['captcha'] = $this->recaptcha->getWidget();
        // $this->data['script_captcha'] = $this->recaptcha->getScriptTag();
        // $recaptcha 	= $this->input->post('g-recaptcha-response');
        // $response 	= $this->recaptcha->verifyResponse($recaptcha);

        //validate form input
        $this->form_validation->set_rules('identity', 'Identity', 'callback_identity_check');
        $this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');
        // $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required');
        $this->form_validation->set_message('required', '{field} mohon diisi');

        // jika form_validation gagal dijalankan dan response recaptcha juga gagal maka akan diarahkan kembali ke halaman login
        if ($this->form_validation->run() == FALSE) {
            // set pesan error dari ion_auth
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            // email atau username yang diatur pada config ion_auth
            $this->data['identity'] = array(
                'name'     => 'identity',
                'id'    => 'identity',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array(
                'name'     => 'password',
                'id'       => 'password',
                'class' => 'form-control',
            );

            // _render_page == view
            $this->_render_page('front/auth/login', $this->data);
        } else {
            // cek user login dan menekan tombol remember me
            $remember = (bool) $this->input->post('remember');

            // cek keberhasilan login dengan function login_front
            if ($this->ion_auth->login_front($this->input->post('identity'), $this->input->post('password'), $remember)) {
                //set message dan redirect ke home apabila berhasil login
                $this->session->set_flashdata('message', '<div class="col-lg-12"><div class="alert alert-block alert-success"><i class="ace-icon fa fa-bullhorn green"></i> Login Berhasil</div></div>');
                redirect(base_url(), 'refresh');
            } else {
                //set message dan redirect ke form login apabila gagal login
                $this->session->set_flashdata('message', $this->ion_auth->errors(''));
                redirect('login', 'refresh');
            }
        }
    }

    public function logout()
    {
        $logout = $this->ion_auth->logout();
        redirect(base_url(), 'refresh');
    }

    // cek identity
    public function identity_check($str)
    {
        $this->load->model('Ion_auth_model');
        if ($this->ion_auth_model->identity_check($str)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('identity_check', 'Username tidak ditemukan');
            return FALSE;
        }
    }


    // Aktivasi user
    public function activate($id, $code = false)
    {
        if ($code !== false) {
            $activation = $this->ion_auth->activate($id, $code);
        }
        if ($activation) {
            // jika berhasil aktivasi akun
            $this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
				<i class="ace-icon fa fa-bullhorn green"></i> Akun berhasil diaktifkan
			</div>');
            redirect("login", 'refresh');
        } else {
            $this->session->set_flashdata('message', '
			<div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
				<i class="ace-icon fa fa-bullhorn green"></i> Akun gagal diaktifkan
			</div>');
            redirect("login", 'refresh');
        }
    }

    public function forgot_password()
    {
        // cek identity nya apakah email atau username untuk dijadikan bagian lupa password
        if ($this->config->item('identity', 'ion_auth') != 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_username_label'), 'required');
        } else {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->data['type'] = $this->config->item('identity', 'ion_auth');
            // setup the input
            $this->data['identity'] = array(
                'name' => 'identity',
                'id' => 'identity',
            );

            echo "<script>alert('Email harus diisi!');history.go(-1)</script>";
        } else {
            // perintah bawaan ion_auth untuk identity (jangan diotak-atik)
            $identity_column = $this->config->item('identity', 'ion_auth');
            // siapkan identity
            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

            // cek ketersediaan data akun $identity di tabel
            if (empty($identity)) {
                echo "<script>alert('Email tidak ditemukan!');history.go(-1)</script>";
            } else {
                // jalankan method forgotten_password untuk lanjut ke proses lupa password
                $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});
                // jika berhasil
                if ($forgotten) {
                    echo "<script>alert('Reset Password berhasil, silahkan cek email Anda!');history.go(-1)</script>";
                } else {
                    echo "<script>alert('Reset Password gagal, silahkan dicoba kembali!');history.go(-1)</script>";
                }
            }
        }
    }

    // Tahap lanjutan dari lupa password -> reset password
    public function reset_password($code = NULL)
    {
        // cek kode reset_password apakah ada/tidak di tabel
        if (!$code) {
            show_404();
        }

        $user = $this->ion_auth->forgotten_password_check($code);

        // cek akun yang punya kode reset menggunakan $user diatas dengan function forgotten_password_check
        if ($user) {
            //siapkan form_validation bagian password baru dan ulangi password
            $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

            if ($this->form_validation->run() == false) {

                // set the flash data error message if there is one
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                // tampilkan settingan panjang minimum password
                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');

                // siapkan data untuk insert password baru
                $this->data['new_password'] = array(
                    'name' => 'new',
                    'id'   => 'new',
                    'type' => 'password',
                    'class' => 'form-control',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                );
                $this->data['new_password_confirm'] = array(
                    'name'    => 'new_confirm',
                    'id'      => 'new_confirm',
                    'type'    => 'password',
                    'class' => 'form-control',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                );
                $this->data['user_id'] = array(
                    'name'  => 'user_id',
                    'id'    => 'user_id',
                    'type'  => 'hidden',
                    'value' => $user->id_user,
                );

                // perintah csrf bawaan dari ion_auth(jangan diotak-atik)
                $this->data['csrf'] = $this->_get_csrf_nonce();
                // code reset password yang ada di db dan url
                $this->data['code'] = $code;

                $this->_render_page('front/auth/reset_password', $this->data);
            } else {
                // cek validitas request data yang diminta
                if ($this->_valid_csrf_nonce() === FALSE || $user->id_user != $this->input->post('user_id')) {
                    // hapus forgotten_password_code di tabel
                    $this->ion_auth->clear_forgotten_password_code($code);
                    show_error($this->lang->line('error_csrf'));
                } else {
                    // siapkan $identity untuk dimasukkan pada bagian $change
                    $identity = $user->{$this->config->item('identity', 'ion_auth')};

                    // siapkan $change untuk mereset password yang sudah final dan sebagai pengecekan berhasil/tidaknya password baru
                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));
                    if ($change) {
                        $this->session->set_flashdata('message', '<div class="alert alert-success alert">Reset Password Berhasil</div>');
                        redirect("login", 'refresh');
                    } else {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        redirect('auth/reset_password/' . $code, 'refresh');
                    }
                }
            }
        } else {
            // jika tidak ditemukan/ invalid
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("login", 'refresh');
        }
    }

    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key   = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public function _valid_csrf_nonce()
    {
        if (
            $this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
            $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')
        ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // sama seperti view
    public function _render_page($view, $data = null, $returnhtml = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml) return $view_html; //This will return html on 3rd argument being true
    }
}
