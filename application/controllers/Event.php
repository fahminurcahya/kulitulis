<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Event extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        /* memanggil model untuk ditampilkan pada masing2 modul */
        $this->load->model('EventModel');
        $this->load->model('CompanyModel');
        $this->load->model('EbookModel');

        /* memanggil function dari masing2 model yang akan digunakan */
        $this->data['event_data']                     = $this->EventModel->get_all_sidebar();
        $this->data['ebook_side']         = $this->EbookModel->get_all_new_home();
        $this->data['company_data']             = $this->CompanyModel->get_by_company();
    }

    public function read($id)
    {
        /* mengambil data berdasarkan id */
        $row = $this->EventModel->get_by_id_front($id);

        /* melakukan pengecekan data, apabila ada maka akan ditampilkan */
        if ($row) {
            /* memanggil function dari masing2 model yang akan digunakan */
            $this->data['event']            = $this->EventModel->get_by_id_front($id);
            $this->data['event_lainnya']    = $this->EventModel->get_all_random();

            $this->data['title'] = $row->judul_event;

            /* memanggil view yang telah disiapkan dan passing data dari model ke view*/
            $this->load->view('front/event/body', $this->data);
        } else {
            $this->session->set_flashdata('message', '
				<div class="col-lg-12">
					<div class="alert alert-dismissible alert-danger">
        		<button type="button" class="close" data-dismiss="alert">&times;</button>event tidak ditemukan</b>
					</div>
				</div>
			');
            redirect(base_url());
        }
    }
}
