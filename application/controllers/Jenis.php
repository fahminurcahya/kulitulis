<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('EventModel');
        $this->load->model('CompanyModel');
        $this->load->model('EbookModel');
        $this->load->model('JenisModel');


        /* memanggil function dari masing2 model yang akan digunakan */
        $this->data['event_data']                     = $this->EventModel->get_all_sidebar();
        $this->data['ebook_side']         = $this->EbookModel->get_all_new_home();
        $this->data['company_data']             = $this->CompanyModel->get_by_company();
    }

    private $limit = 9;

    public function read($id)
    {
        $this->load->helper(array('clean'));
        $this->data['segment'] = count($this->uri->segment_array());

        $segment  = count($this->uri->segment_array());
        $segments = $this->uri->segment_array();

        $offset = 0;

        if ($segment == 3 || ($segment == 4 && is_numeric($segments[4]))) {
            $this->data['title'] = strtoupper(clean2($this->uri->segment(3)));
            if ($segment == 4)
                $offset = $segments[4];

            $this->data['ebook_row'] = $this->JenisModel->get_list_by_jenis($segments[3], $this->limit, $offset)->row();

            $this->data['ebook'] = $this->JenisModel->get_list_by_jenis($segments[3], $this->limit, $offset);

            $this->data['pagination'] = $this->generate_paging($this->JenisModel->get_by_jenis_nr($segments[3]), base_url() . 'jenis/read/' . $segments[3],  4);
        } else if ($segment == 4 || ($segment == 5 && is_numeric($segments[5]))) {
            $this->data['title'] = strtoupper(clean2($this->uri->segment(4)));

            if ($segment == 5)
                $offset = $segments[5];

            $this->data['ebook_row'] = $this->JenisModel->get_list_by_kategori($segments[4], $this->limit, $offset)->row();

            $this->data['ebook'] = $this->JenisModel->get_list_by_kategori($segments[4], $this->limit, $offset);

            $this->data['pagination'] = $this->generate_paging($this->JenisModel->get_by_kategori_nr($segments[4]), base_url() . 'jenis/read/' . $segments[3] . '/' . $segments[4],  5);
        }
        $this->load->view('front/jenis/body', $this->data);
    }

    function generate_paging($numRows, $url, $uriSegment, $suffix = '')
    {
        $this->load->library('Pagination');

        $config['full_tag_open']         = '<nav><ul class="pagination">';
        $config['full_tag_close']     = '</ul></nav>';
        $config['num_tag_open']         = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']         = '</span></li>';
        $config['cur_tag_open']         = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']         = '<span class="sr-only">(current)</span></span></li>';
        $config['next_link']        = "Selanjutnya";
        $config['next_tag_open']         = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']     = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_link']        = "Sebelumnya";
        $config['prev_tag_open']         = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']     = '</span></li>';
        $config['first_link']       = "Awal";
        $config['first_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_link']        = 'Terakhir';
        $config['last_tag_open']         = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']     = '</span></li>';

        $config['base_url'] = $url;
        $config['total_rows'] = $numRows;
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = $uriSegment;
        $config['suffix'] = $suffix;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }
}
