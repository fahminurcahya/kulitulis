<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image"><img src="<?php echo base_url() ?>assets/images/user/user.jpg" class="img-circle" alt="User Image" /></div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('nama_lengkap'); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="header">MENU UTAMA</li>
            <li <?php if ($this->uri->segment(2) == "dashboard") {
                    echo "class='active'";
                } ?>>
                <a href="<?php echo base_url('dashboard') ?>">
                    <i class="fa fa-home"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('/') ?>">
                    <i class="fa fa-globe"></i> <span>Halaman Depan</span>
                </a>
            </li>

            <!-- menu untuk penulis dan admin  -->
            <?php if (!$this->ion_auth->is_superadmin()) : ?>
                <li <?php if ($this->uri->segment(2) == "tulisan") {
                        echo "class='active'";
                    } ?>>
                    <a href='#'><i class='fa fa-newspaper-o'></i><span> Tulisan </span><i class='fa fa-angle-left pull-right'></i></a>
                    <ul class='treeview-menu'>
                        <!-- submenu khusus penulis  -->
                        <?php if ($this->ion_auth->is_penulis()) : ?>
                            <li <?php if ($this->uri->segment(2) == "penulis" && $this->uri->segment(3) == "") {
                                    echo "class='active'";
                                } ?>><a href='<?php echo base_url('tulisan/create') ?>'><i class='fa fa-circle-o'></i> Tulisan Baru </a></li>
                            <li <?php if ($this->uri->segment(2) == "penulis" && $this->uri->segment(3) == "") {
                                    echo "class='active'";
                                } ?>><a href='<?php echo base_url('tulisan/list/draft_list') ?>'><i class='fa fa-circle-o'></i> Draft </a></li>
                            <li <?php if ($this->uri->segment(2) == "penulis" && $this->uri->segment(3) == "") {
                                    echo "class='active'";
                                } ?>><a href='<?php echo base_url('tulisan/list/dikirim') ?>'><i class='fa fa-circle-o'></i> Sudah Dikirim </a></li>
                        <?php endif ?>
                        <!-- end submenu khusus penulis  -->
                        <?php if ($this->ion_auth->is_admin()) : ?>
                            <li <?php if ($this->uri->segment(4) == "baru-perbaikan") {
                                    echo "class='active'";
                                } ?>><a href='<?php echo base_url('admin/tulisan/list/baru-perbaikan') ?>'><i class='fa fa-circle-o'></i> List Tulisan Baru </a></li>
                            <li <?php if ($this->uri->segment(4) == "approve") {
                                    echo "class='active'";
                                } ?>><a href='<?php echo base_url('admin/tulisan/list/approve') ?>'><i class='fa fa-circle-o'></i> Sudah Disetujui </a></li>
                            <li <?php if ($this->uri->segment(4) == "paid") {
                                    echo "class='active'";
                                } ?>><a href='<?php echo base_url('admin/tulisan/list/paid') ?>'><i class='fa fa-circle-o'></i> Sudah Dibayar </a></li>
                        <?php endif ?>
                    </ul>
                </li>
            <?php endif ?>
            <!-- end menu penulis dan admin  -->


            <!-- menu khusus admin dan superadmin  -->
            <?php if (!$this->ion_auth->is_penulis()) : ?>
                <li <?php if ($this->uri->segment(2) == "slider" || $this->uri->segment(2) == "ebook" || $this->uri->segment(2) == "event" || $this->uri->segment(2) == "jenis" || $this->uri->segment(2) == "kategori" || $this->uri->segment(2) == "users") {
                        echo "class='active'";
                    } ?>>
                    <a href='#'><i class='fa fa-edit'></i><span> Master </span><i class='fa fa-angle-left pull-right'></i></a>
                    <ul class='treeview-menu'>
                        <li <?php if ($this->uri->segment(2) == "slider") {
                                echo "class='active'";
                            } ?>><a href='<?php echo base_url('admin/slider') ?>'><i class='fa fa-circle-o'></i> Master Slider </a></li>
                        <li <?php if ($this->uri->segment(2) == "ebook" && $this->uri->segment(3) == "") {
                                echo "class='active'";
                            } ?>><a href='<?php echo base_url('admin/ebook') ?>'><i class='fa fa-circle-o'></i> Master Ebook </a></li>
                        <li <?php if ($this->uri->segment(2) == "event" && $this->uri->segment(3) == "") {
                                echo "class='active'";
                            } ?>><a href='<?php echo base_url('admin/event') ?>'><i class='fa fa-circle-o'></i> Master Event </a></li>
                        <li <?php if ($this->uri->segment(2) == "jenis" && $this->uri->segment(3) == "") {
                                echo "class='active'";
                            } ?>><a href='<?php echo base_url('admin/jenis') ?>'><i class='fa fa-circle-o'></i> Master Jenis Tulisan </a></li>
                        <li <?php if ($this->uri->segment(2) == "kategori" && $this->uri->segment(3) == "") {
                                echo "class='active'";
                            } ?>><a href='<?php echo base_url('admin/kategori') ?>'><i class='fa fa-circle-o'></i> Master Kategori Tulisan </a></li>
                        <!-- sub menu khusus super admin  -->
                        <?php if ($this->ion_auth->is_superadmin()) : ?>
                            <li <?php if ($this->uri->segment(2) == "users") {
                                    echo "class='active'";
                                } ?>><a href='<?php echo base_url('admin/users') ?>'><i class='fa fa-circle-o'></i> Master User </a></li>
                        <?php endif ?>
                        <!-- end submenu khusus superadmin  -->
                    </ul>
                </li>
            <?php endif ?>
            <!-- end menu khusu admin dan super admin  -->

            <li class="header">SETTING</li>
            <?php if ($this->ion_auth->is_superadmin()) : ?>
                <li <?php if ($this->uri->segment(2) == "company") {
                        echo "class='active'";
                    } ?>><a href='<?php echo base_url() ?>admin/company/update'> <i class="fa fa-building"></i> <span>Profil Perusahaan</span> </a> </li>
            <?php else : ?>
                <li class='treeview'>
                    <a href='<?php echo base_url('info/profile') ?>'>
                        <i class='fa fa-user'></i><span> Profile </span>
                    </a>
                </li>
            <?php endif ?>
            <li> <a href='<?php echo base_url() ?>admin/auth/logout'> <i class="fa fa-sign-out"></i> <span>Logout</span> </a> </li>
        </ul>

    </section>
</aside>