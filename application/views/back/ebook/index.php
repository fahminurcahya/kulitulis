<?php $this->load->view('back/meta') ?>
<div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $title ?></h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#"><?php echo $module ?></a></li>
                <li class="active"><?php echo $title ?></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <h4>5 Best Seller</h4>
                            <hr>
                            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                            <div class="table-responsive no-padding">
                                <table id="datatableBest" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="text-align: left">No</th>
                                            <th style="text-align: left">Judul</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- ./col -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <a href="<?php echo base_url('admin/ebook/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                            <a href="#" class="btn btn-default" onclick="reload_table()"><i class="fa fa-refresh"></i> Refresh</a>
                            <hr>
                            <div class="table-responsive no-padding">
                                <table id="datatableEbook" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">No</th>
                                            <th style="text-align: center">Best Seller</th>
                                            <th style="text-align: center">Judul</th>
                                            <th style="text-align: center">Gambar</th>
                                            <th style="text-align: center">Stok</th>
                                            <th style="text-align: center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- ./col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
</div><!-- ./wrapper -->
<?php $this->load->view('back/js') ?>
<!-- DATA TABLES-->
<link href="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url('assets/plugins/') ?>datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var tableEbook;
    $(document).ready(function() {
        tableEbook = $('#datatableEbook').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "aaSorting": [
                [0, null]
            ],
            "lengthMenu": [
                [10, 25, 50, 100, 500, 1000, -1],
                [10, 25, 50, 100, 500, 1000, "Semua"]
            ],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('admin/ebook/ajax_list') ?>",
                "type": "POST",
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": '_all', //all column,
                "orderable": false,
            }, ],
        });

        tableBest = $('#datatableBest').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": false, //Feature control DataTables' server-side processing mode.
            "paging": false,
            "ordering": false,
            "searching": false,
            "info": false,

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('admin/ebook/ajax_list_best') ?>",
                "type": "POST",
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": '_all', //all column,
                "orderable": false,
            }, ],
        });
    });

    function reload_table() {
        tableEbook.ajax.reload(null, false); //reload datatable ajax
    }

    function reload_table_best() {
        tableBest.ajax.reload(null, false); //reload datatable ajax
    }

    function bestSeller(checkeds) {
        if (checkeds.checked) {
            best_seller = '1'
        } else {
            best_seller = '0'
        }
        $.ajax({
            url: "<?php echo site_url('admin/ebook/update_best') ?>",
            type: "post",
            data: {
                "id_ebook": $(checkeds).attr("id"),
                "best_seller": best_seller
            },
            dataType: "json",
            success: function(response) {
                if (response.error) {
                    alert(response.message);
                    reload_table();
                } else {
                    reload_table_best();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // reload_table()
            }
        });
    }
</script>
</body>

</html>