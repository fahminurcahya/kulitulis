<form action="<?= site_url('admin/tulisan/update_action/paid') ?>" method="post">
    <div class="modal-body">
        <?php echo form_input($id_tulisan, $penulis->id_tulisan); ?>
        <div class="form-group"><label>Nama Lengkap</label>
            <?php echo form_input($nama_lengkap, $penulis->nama_lengkap); ?>
        </div>
        <div class="form-group"><label>Nomor Hp</label>
            <?php echo form_input($nomor_hp, $penulis->nomor_hp); ?>
        </div>
        <div class="form-group"><label>Nama Bank</label>
            <?php echo form_input($nama_bank, $penulis->nama_bank); ?>
        </div>
        <div class="form-group"><label>Nomor Rekening</label>
            <?php echo form_input($nomor_rekening, $penulis->nomor_rekening); ?>
        </div>
        <div class="form-group"><label>Atas Nama</label>
            <?php echo form_input($atas_nama, $penulis->atas_nama); ?>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" value="Tolak" class="btn btn-info">Bayar</button>
    </div>
</form>