<?php $this->load->view('back/meta') ?>
<div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $title ?></h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#"><?php echo $module ?></a></li>
                <li class="active"><?php echo $title ?></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <?php echo validation_errors() ?>
                            <?php if ($this->session->flashdata('warning')) {
                                echo $this->session->flashdata('warning');
                            } ?>
                            <?php echo form_open_multipart(); ?>
                            <div class="form-group"><label>Nama Pena</label>
                                <?php echo form_input($nama_pena, $data_tulisan->nama_pena); ?>
                            </div>
                            <div class="form-group"><label>Judul Tulisan</label>
                                <?php echo form_input($judul, $data_tulisan->judul); ?>
                            </div>
                            <div class="form-group"><label>Jenis</label>
                                <?php echo form_dropdown('', $ambil_jenis, $data_tulisan->id_jenis, $id_jenis); ?>
                            </div>
                            <div class="form-group"><label>Sub Kategori</label>
                                <?php echo form_dropdown('', $ambil_kategori, $data_tulisan->id_kategori, $id_kategori); ?><br>
                            </div>
                            <div class="form-group"><label>Sinopsis</label>
                                <?php echo form_textarea($sinopsis, $data_tulisan->sinopsis); ?>
                            </div>
                            <div class="form-group"><label>Isi</label>
                                <?php echo form_textarea($tulisan, $data_tulisan->tulisan); ?>
                            </div>
                            <?php echo form_input($id_tulisan); ?>
                            <button type="button" name="Tolak" id="Tolak" onclick="confirm(<?php echo $data_tulisan->id_tulisan ?>)" class="btn btn-danger"><?php echo $button_reject ?></button>
                            <button type="submit" name="Setujui" id="Setujui" value="Setujui" class="btn btn-success" formaction="<?= site_url('admin/tulisan/update_action/approve') ?>"><?php echo $button_approve ?></button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div><!-- ./col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
</div><!-- ./wrapper -->


<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalLabel">Tolak Tulisan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= site_url('admin/tulisan/update_action/reject') ?>" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" id="idTulisan" name="idTulisan">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Note:</label>
                        <textarea class="form-control" id="note" name="note" required></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" value="Tolak" class="btn btn-success"><?php echo $button_reject ?></button>
            </div>
            </form>

        </div>
    </div>
</div>


<?php $this->load->view('back/js') ?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#tulisan",

        // ===========================================
        // INCLUDE THE PLUGIN
        // ===========================================

        plugins: [
            'wordcount', 'fullscreen', 'noneditable',
        ],

        noneditable_noneditable_class: 'mceNonEditable',

        // ===========================================
        // PUT PLUGIN'S BUTTON on the toolbar
        // ===========================================

        toolbar: "fullscreen | wordcount",
        menubar: false,
        // ===========================================
        // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
        // ===========================================
        setup: function(editor) {
            editor.setMode("readonly")
        },
        relative_urls: false,
        remove_script_host: false,
        convert_urls: true,

    });

    tinymce.init({
        selector: "#sinopsis",



        // ===========================================
        // INCLUDE THE PLUGIN
        // ===========================================

        plugins: [
            'fullscreen', 'noneditable',
        ],

        toolbar: "fullscreen",
        menubar: false,
        // ===========================================
        // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
        // ===========================================
        setup: function(editor) {
            editor.setMode("readonly")
        },
        relative_urls: false,
        remove_script_host: false,
        convert_urls: true
    });

    function confirm($id) {
        $('#confirmModal').modal('show');
        $('#idTulisan').val($id);
    }
</script>
</body>

</html>