<?php $this->load->view('back/meta') ?>
<div class="wrapper">
    <?php $this->load->view('back/navbar') ?>
    <?php $this->load->view('back/sidebar') ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $title ?></h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#"><?php echo $module ?></a></li>
                <li class="active"><?php echo $title ?></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <?php echo validation_errors() ?>
                            <?php if ($this->session->flashdata('message')) {
                                echo $this->session->flashdata('message');
                            } ?>
                            <?php echo form_open_multipart(); ?>
                            <div class="form-group"><label>Nama Pena</label>
                                <?php echo form_input($nama_pena); ?>
                            </div>
                            <div class="form-group"><label>Judul Tulisan</label>
                                <?php echo form_input($judul); ?>
                            </div>
                            <div class="form-group"><label>Jenis</label>
                                <?php echo form_dropdown('', $ambil_jenis, '', $id_jenis); ?>
                            </div>
                            <div class="form-group"><label>Sub Kategori</label>
                                <?php echo form_dropdown('', array('' => '- Pilih Kategori -'), '', $id_kategori); ?>
                            </div>
                            <div class="form-group"><label>Sinopsis</label>
                                <?php echo form_textarea($sinopsis); ?>
                            </div>
                            <div class="form-group"><label>Isi (100 - 5000 Kata)</label>
                                <?php echo form_textarea($tulisan); ?>
                            </div>
                            <?php echo form_input($id_user); ?>
                            <?php echo form_input($id_tulisan); ?>
                            <button type="submit" name="publish" id="publish" value="publish" class="btn btn-success" formaction="<?= site_url('admin/Penulis/publish') ?>"><?php echo $button_submit ?></button>
                            <button type="submit" name="draft" id="draft" value="draft" class="btn btn-warning" formaction="<?= site_url('admin/Penulis/draft') ?>"><?php echo $button_draft ?></button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div><!-- ./col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
</div><!-- ./wrapper -->
<?php $this->load->view('back/js') ?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        document.getElementById("publish").disabled = true;
    });


    function disablebutton() {
        document.getElementById("publish").disabled = true;
    }

    function enablebutton() {
        document.getElementById("publish").disabled = false;
    }

    tinymce.init({
        selector: "#tulisan",

        // ===========================================
        // INCLUDE THE PLUGIN
        // ===========================================

        plugins: [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime table contextmenu paste ",
            'wordcount', 'fullscreen'
        ],

        // ===========================================
        // PUT PLUGIN'S BUTTON on the toolbar
        // ===========================================

        toolbar: "insertfile undo redo | fullscreen | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | wordcount",
        menubar: 'view',
        // ===========================================
        // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
        // ===========================================

        relative_urls: false,
        remove_script_host: false,
        convert_urls: true,

        setup: function(ed) {
            ed.on('keyup', function(e) {
                var wordcount = tinymce.activeEditor.plugins.wordcount;
                // console.log(wordcount.getCount());
                var totalkata = wordcount.getCount();

                if (totalkata > 100 && totalkata <= 5000) {
                    enablebutton();
                } else {
                    disablebutton();
                }
            });
        }

    });

    tinymce.init({
        selector: "#sinopsis",

        // ===========================================
        // INCLUDE THE PLUGIN
        // ===========================================

        plugins: [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime table contextmenu paste "
        ],

        // ===========================================
        // PUT PLUGIN'S BUTTON on the toolbar
        // ===========================================

        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",

        // ===========================================
        // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
        // ===========================================

        relative_urls: false,
        remove_script_host: false,
        convert_urls: true
    });

    function tampilKategori() {
        id_jenis = document.getElementById("id_jenis").value;
        $.ajax({
            url: "<?php echo base_url(); ?>admin/penulis/pilih_kategori/" + id_jenis + "",
            success: function(response) {
                $("#id_kategori").html(response);
            },
            dataType: "html"
        });
        return false;
    }
</script>
</body>

</html>