<?php $this->load->view('back/meta') ?>
<?php $this->load->view('back/navbar') ?>
<?php $this->load->view('back/sidebar') ?>
<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Dashboard</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <?php if ($this->ion_auth->is_penulis()) : ?>
                <?php if ($this->session->flashdata('warning')) {
                    echo $this->session->flashdata('warning');
                } ?>
            <?php endif ?>

            <!-- penampilan total record -->
            <div class="row">
                <?php if ($this->ion_auth->is_penulis()) : ?>
                    <div class='col-lg-3'>
                        <div class='small-box bg-yellow'>
                            <div class='inner'>
                                <h3> <?php echo $jumlah_dikirim ?> </h3>
                                <p><b>Tulisan Dikirim</b></p>
                            </div>
                            <div class='icon'><i class='fa fa-paper-plane'></i></div>
                            <a href='<?php echo base_url('tulisan/list/draft_list') ?>' class='small-box-footer'>Selengkapnya <i class='fa fa-arrow-circle-right'></i></a>
                        </div>
                    </div>
                    <div class='col-lg-3'>
                        <div class='small-box bg-red'>
                            <div class='inner'>
                                <h3> <?php echo $jumlah_ditolak ?> </h3>
                                <p><b>Tulisan Ditolak</b></p>
                            </div>
                            <div class='icon'><i class='fa fa-minus-square'></i></div>
                            <a href='<?php echo base_url('tulisan/list/draft_list') ?>' class='small-box-footer'>Selengkapnya <i class='fa fa-arrow-circle-right'></i></a>
                        </div>
                    </div>
                    <div class='col-lg-3'>
                        <div class='small-box bg-blue'>
                            <div class='inner'>
                                <h3> <?php echo $jumlah_disetujui ?> </h3>
                                <p><b>Tulisan Disetujui</b></p>
                            </div>
                            <div class='icon'><i class='fa fa-check-square'></i></div>
                            <a href='<?php echo base_url('tulisan/list/draft_list') ?>' class='small-box-footer'>Selengkapnya <i class='fa fa-arrow-circle-right'></i></a>
                        </div>
                    </div>
                    <div class='col-lg-3'>
                        <div class='small-box bg-green'>
                            <div class='inner'>
                                <h3> <?php echo $jumlah_dibayar ?> </h3>
                                <p><b>Tulisan Dibayar</b></p>
                            </div>
                            <div class='icon'><i class='fa fa-money'></i></div>
                            <a href='<?php echo base_url('tulisan/list/draft_list') ?>' class='small-box-footer'>Selengkapnya <i class='fa fa-arrow-circle-right'></i></a>
                        </div>
                    </div>
                <?php elseif ($this->ion_auth->is_admin()) : ?>
                    <div class='col-lg-6'>
                        <div class='small-box bg-green'>
                            <div class='inner'>
                                <h3> <?php echo $jumlah_ebook ?> </h3>
                                <p><b>Ebook</b></p>
                            </div>
                            <div class='icon'><i class='fa fa-paper-plane'></i></div>
                            <a href='<?php echo base_url('admin/ebook') ?>' class='small-box-footer'>Selengkapnya <i class='fa fa-arrow-circle-right'></i></a>
                        </div>
                    </div>
                    <div class='col-lg-6'>
                        <div class='small-box bg-blue'>
                            <div class='inner'>
                                <h3> <?php echo $jumlah_event ?> </h3>
                                <p><b>Event</b></p>
                            </div>
                            <div class='icon'><i class='fa fa-minus-square'></i></div>
                            <a href='<?php echo base_url('admin/event') ?>' class='small-box-footer'>Selengkapnya <i class='fa fa-arrow-circle-right'></i></a>
                        </div>
                    </div>
                <?php elseif ($this->ion_auth->is_superadmin()) : ?>
                    <div class='col-lg-4'>
                        <div class='small-box bg-green'>
                            <div class='inner'>
                                <h3> <?php echo $jumlah_ebook ?> </h3>
                                <p><b>Ebook</b></p>
                            </div>
                            <div class='icon'><i class='fa fa-paper-plane'></i></div>
                            <a href='<?php echo base_url('admin/ebook') ?>' class='small-box-footer'>Selengkapnya <i class='fa fa-arrow-circle-right'></i></a>
                        </div>
                    </div>
                    <div class='col-lg-4'>
                        <div class='small-box bg-blue'>
                            <div class='inner'>
                                <h3> <?php echo $jumlah_event ?> </h3>
                                <p><b>Event</b></p>
                            </div>
                            <div class='icon'><i class='fa fa-minus-square'></i></div>
                            <a href='<?php echo base_url('admin/event') ?>' class='small-box-footer'>Selengkapnya <i class='fa fa-arrow-circle-right'></i></a>
                        </div>
                    </div>
                    <div class='col-lg-4'>
                        <div class='small-box bg-yellow'>
                            <div class='inner'>
                                <h3> <?php echo $jumlah_user ?> </h3>
                                <p><b>Penulis</b></p>
                            </div>
                            <div class='icon'><i class='fa fa-minus-square'></i></div>
                            <a href='<?php echo base_url('admin/users') ?>' class='small-box-footer'>Selengkapnya <i class='fa fa-arrow-circle-right'></i></a>
                        </div>
                    </div>
                <?php endif ?>
            </div>
            <div class="row">
                <?php if ($this->ion_auth->is_penulis()) : ?>
                    <div class="col-lg-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <h4>10 Draf Terbaru</h4>
                                <hr>
                                <div class="table-responsive no-padding">
                                    <table id="datatable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center">No</th>
                                                <th style="text-align: center">Judul</th>
                                                <th style="text-align: center">Jenis</th>
                                                <th style="text-align: center">Kategori</th>
                                                <th style="text-align: center">Nama Lengkap</th>
                                                <th style="text-align: center">Nama Pena</th>
                                                <th style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div><!-- ./col -->
                <?php elseif ($this->ion_auth->is_admin()) : ?>
                    <div class="col-lg-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <h4>10 Tulisan Terbaru</h4>
                                <hr>
                                <div class="table-responsive no-padding">
                                    <table id="datatable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center">No</th>
                                                <th style="text-align: center">Judul</th>
                                                <th style="text-align: center">Jenis</th>
                                                <th style="text-align: center">Kategori</th>
                                                <th style="text-align: center">Nama</th>
                                                <th style="text-align: center">Nama Pena</th>
                                                <th style="text-align: center">Tanggal Kirim</th>
                                                <th style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div><!-- ./col -->
                <?php endif ?>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('back/footer') ?>
</div><!-- ./wrapper -->
<?php $this->load->view('back/js') ?>
<!-- DATA TABLES-->
<link href="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url('assets/plugins/') ?>datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/') ?>datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
        <?php if ($this->ion_auth->is_penulis()) : ?>
            table = $('#datatable').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": false, //Feature control DataTables' server-side processing mode.
                "paging": false,
                "ordering": false,
                "info": false,

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('admin/dashboard/ajax_list/D') ?>",
                    "type": "POST"
                },

                //Set column definition initialisation properties.
                "columnDefs": [{
                    "targets": '_all', //all column,
                    "orderable": false,
                }, ],
            });
        <?php elseif ($this->ion_auth->is_admin()) : ?>
            table = $('#datatable').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": false, //Feature control DataTables' server-side processing mode.
                "paging": false,
                "ordering": false,
                "info": false,

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('admin/dashboard/ajax_list/S') ?>",
                    "type": "POST"
                },

                //Set column definition initialisation properties.
                "columnDefs": [{
                    "targets": '_all', //all column,
                    "orderable": false,
                }, ],
            });
        <?php endif ?>
    });
</script>
</body>

</html>