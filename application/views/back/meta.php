<!DOCTYPE html>
<html>

<head>
    <title>dashboard</title>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url('assets/template/backend/') ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url('assets/plugins/') ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Jquery -->
    <script src="<?php echo base_url('assets/plugins/') ?>jquery/jquery-3.3.1.js"></script>
    <!-- Theme style -->
    <link href="<?php echo base_url('assets/template/backend/') ?>dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url('assets/template/backend/') ?>dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/fav.jpg') ?>" />
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Kalam:wght@700&family=Open+Sans:ital,wght@0,400;1,300&family=Roboto+Condensed&display=swap');

        div.mce-fullscreen {
            z-index: 1050;
        }
    </style>
</head>

<body class="skin-blue sidebar-mini">