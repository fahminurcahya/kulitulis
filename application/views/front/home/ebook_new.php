<hr>
<div class="row justify-content-between">
    <div class="col-2" style="text-align:left;">
        <h4 align="center">Ebook Terbaru</h4>
    </div>
</div>
<hr>
<div class="row row-horizon flex-nowrap">
    <?php foreach ($ebook_new_data as $ebook) { ?>
        <div class="col-xl-3 col-lg-4 col-md-12 col-sm-6 col-xs-12">
            <div class="card mb-4 box-shadow" style="min-width: 200px;">
                <a href="<?php echo base_url("ebook/$ebook->slug_ebook ") ?>" style="padding-left :40px; padding-right:40px;">
                    <?php
                    if (empty($ebook->foto)) {
                        echo "<img class='card-img-top' src='" . base_url() . "assets/images/no_image_thumb.png'>";
                    } else {
                        echo "<img class='card-img-top' src='" . base_url() . "assets/images/ebook/" . $ebook->foto . '_thumb' . $ebook->foto_type . "'> ";
                    }
                    ?>
                </a>
                <div class="card-body">
                    <a href="<?php echo base_url("ebook/$ebook->slug_ebook ") ?>">
                        <p class="card-text"><b><?php echo character_limiter($ebook->judul, 50) ?></b></p>
                    </a>
                    <br>
                    <p align="center">
                        <strike><b>Rp <?php echo number_format($ebook->harga_normal) ?></b></strike><br>
                        <b>Rp <?php echo number_format($ebook->harga_diskon) ?></b>
                        <font style="font-size:15px"><span class="badge badge-pill badge-primary"><?= $ebook->diskon ?> % OFF</span></font>
                    </p>
                    <p align="center">
                        <a href="<?php echo base_url('ebook/') . $ebook->slug_ebook ?>">
                            <button class="btn btn btn-danger"><i class="fa fa-eye"></i> Detail</button>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    <?php } ?>
</div>