<hr>
<h4 align="center">Event</h4>
<hr>
<?php foreach ($event_new_data as $event) { ?>
    <div class="row">
        <div class="col-sm-4">
            <a href="<?php echo base_url("event/read/$event->slug_event ") ?>">
                <?php
                if (empty($event->foto)) {
                    echo "<img class='img-thumbnail' src='" . base_url() . "assets/images/no_image_thumb.png'>";
                } else {
                    echo " <img class='img-thumbnail' src='" . base_url() . "assets/images/event/" . $event->foto . '_thumb' . $event->foto_type . "'> ";
                }
                ?>
            </a>
        </div>
        <div class="col-sm-8">
            <h5><a href="<?php echo base_url("event/read/$event->slug_event ") ?>"><?php echo character_limiter($event->judul_event, 100) ?></a></h5>
            <i class="fa fa-calendar"></i> <?php echo date("j F Y", strtotime($event->created_at)); ?>
            <p><?php echo character_limiter($event->desc_event, 350) ?></p>
            <p align="right">
                <a href="<?php echo base_url("event/read/$event->slug_event ") ?>">
                    <button type="button" name="button" class="btn btn-sm btn-primary">Selengkapnya</button>
                </a>
            </p>
        </div>
    </div>
    <br>
<?php } ?>
