<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>

<div class="container-fluid">
    <div class="row"><?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?></div>
    <?php $this->load->view('front/home/slider'); ?>
    <?php $this->load->view('front/home/ebook_best'); ?>
    <?php $this->load->view('front/home/ebook_new'); ?>
    <?php $this->load->view('front/home/event_new'); ?>
    <?php $this->load->view('front/footer'); ?>
</div>