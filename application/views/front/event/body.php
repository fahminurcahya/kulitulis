<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Event</a></li>
                    <li class="breadcrumb-item active"><?php echo $event->judul_event ?></li>
                </ol>
            </nav>
        </div>
        <div class="col-lg-9">
            <h1><?php echo $event->judul_event ?></h1>
            <hr>
            <div class="row">
                <div class="col-lg-12" align="center">
                    <?php
                    if (empty($event->foto)) {
                        echo "<img class='img-fluid' src='" . base_url() . "assets/images/no_image_thumb.png' width='400' height='400'>";
                    } else {
                        echo " <img class='img-fluid' src='" . base_url() . "assets/images/event/" . $event->foto . $event->foto_type . "' class='img-responsive' title='$event->judul_event' alt='$event->judul_event' id='myImg'> ";
                    }
                    ?>
                    <br>
                </div>
            </div>
            <p><i class="fa fa-calendar"></i> <?php echo date("j F Y", strtotime($event->created_at)); ?></p>

            <div class="row">
                <div class="col-lg-12">
                    <p><?php echo $event->desc_event ?></p>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-lg-12">
                    <h4>Lihat Juga</h4>
                    <hr>
                    <?php foreach ($event_lainnya as $lainnya) { ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="thumbnail">
                                    <?php
                                    if (empty($lainnya->foto)) {
                                        echo "<img class='img-fluid' src='" . base_url() . "assets/images/no_image_thumb.png'>";
                                    } else {
                                        echo " <img class='img-fluid' src='" . base_url() . "assets/images/event/" . $lainnya->foto . '_thumb' . $lainnya->foto_type . "'> ";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="caption">
                                    <h5><a href="<?php echo base_url("event/read/$lainnya->slug_event ") ?>"><?php echo character_limiter($lainnya->judul_event, 100) ?></a></h5>
                                    <i class="fa fa-calendar"></i> <?php echo date("j F Y", strtotime($lainnya->created_at)); ?>
                                    <p><?php echo character_limiter($lainnya->desc_event, 150) ?></p>
                                    <p align="right">
                                        <a href="<?php echo base_url("event/read/$lainnya->slug_event ") ?>">
                                            <button type="button" name="button" class="btn btn-sm btn-primary">Selengkapnya</button>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div><br>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php $this->load->view('front/sidebar'); ?>
    </div>

    <?php $this->load->view('front/footer'); ?>
</div>