<?php foreach ($event_data as $event) { ?>
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-4">
            <a href="<?php echo base_url('event/read/') . $event->slug_event ?>">
                <img class="img-fluid" src="<?php echo base_url('assets/images/event/') . $event->foto . $event->foto_type ?>">
            </a>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-8">
            <a href="<?php echo base_url('event/read/') . $event->slug_event ?>">
                <h5><?php echo character_limiter($event->judul_event, '25') ?></h5>
            </a>
        </div>
    </div><br>
<?php } ?>
