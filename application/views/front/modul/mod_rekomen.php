<?php foreach ($ebook_side as $ebook_side) { ?>
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-6">
            <a href="<?php echo base_url('ebook/') . $ebook_side->slug_ebook ?>">
                <img class="img-thumbnail" src="<?php echo base_url('assets/images/ebook/') . $ebook_side->foto . $ebook_side->foto_type ?>">
            </a>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-6">
            <h5><a href="<?php echo base_url('ebook/') . $ebook_side->slug_ebook ?>"><?php echo character_limiter($ebook_side->judul, '25') ?></a></h5>
            <strike><b>Rp <?php echo number_format($ebook_side->harga_normal) ?></b></strike><br>
            <b>Rp <?php echo number_format($ebook_side->harga_diskon) ?></b>
            <font style="font-size:15px"><span class="badge badge-pill badge-primary"><?php echo $ebook_side->diskon ?>% OFF</span></font>
        </div>
    </div><br>
<?php } ?>
