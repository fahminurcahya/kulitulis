<div id='cssmenu' class="align-center">
    <ul>
        <li style="padding-right : 100px"><a href='<?php echo base_url() ?>'><img src="<?php echo base_url() . 'assets/images/company/' . $company_data->logo . $company_data->logo_type; ?>" width="120px" alt="<?php echo $company_data->company_name ?>" title="<?php echo $company_data->company_name ?>"></a></li>
        <li><a href="<?php echo base_url('ebook/katalog') ?>"></i> Katalog</a></li>

        <li class='active'><a href='#'><i class="fa fa-tags"></i> Jenis</a>
            <ul>
                <?php
                $sql = $this->db->query("SELECT * FROM tbl_jenis ORDER BY nama_jenis"); // Memanggil kategori/ top kategori
                $data = $sql->result();
                foreach ($data as $row) {
                    $id_jenis = $row->id_jenis;
                    echo '
          <li><a href="' . base_url('jenis/read/') . $row->slug_jenis . '">' . $row->nama_jenis . ' </a>
            <ul>';

                    $sql2   =  $this->db->query("SELECT * FROM tbl_kategori WHERE id_jenis = '$id_jenis' ORDER BY nama_kategori "); // Memanggil kategori/ middle kategori
                    $data2  = $sql2->result();
                    foreach ($data2 as $row2) {
                        $id_kategori = $row2->id_kategori;
                        echo '
              <li><a href="' . base_url('jenis/read/') . $row->slug_jenis . '/' . $row2->slug_kategori . '">' . $row2->nama_kategori . '</a>';
                    }
                    echo '
          </ul>';
                }
                echo '
        </li>';
                ?>
            </ul>
        </li>
        <li>
            <a href="#">
                <?php echo form_open('ebook/cari_ebook') ?>
                <input class="form-control mr-md-2" type="text" name="cari" size="50" placeholder="Cari Ebook">
                <?php echo form_close() ?>
            </a>
        </li>

        <li><a href="<?php echo base_url('register') ?>"><i class="fa fa-user"></i> Register / Login</a></li>
    </ul>
</div>
<br>