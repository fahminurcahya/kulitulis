<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Ebook</a></li>
                    <li class="breadcrumb-item active"><?php echo $ebook->judul ?></li>
                </ol>
            </nav>
        </div>
        <!-- Kolom kiri -->
        <div class="col-lg-9">
            <h1><?php echo $ebook->judul ?></h1>
            <hr>
            <div class="row">
                <div class="col-sm-5" align="center">
                    <?php
                    if (empty($ebook->foto)) {
                        echo "<img class='img-thumbnail' src='" . base_url() . "assets/images/no_image_thumb.png' width='400' height='400'>";
                    } else {
                        echo "
						<a href='" . base_url() . "assets/images/ebook/" . $ebook->foto . $ebook->foto_type . "'>
						<img data-action='zoom' class='img-thumbnail' src='" . base_url() . "assets/images/ebook/" . $ebook->foto . '_thumb' . $ebook->foto_type . "' title='$ebook->judul' alt='$ebook->judul' width='400' height='400'>
						</a>";
                    }
                    ?>
                    <br>
                </div>
                <div class="col-sm-7">
                    <p>
                    <h4>Spesifikasi ebook</h4>
                    </p>
                    <hr>
                    <p>Penulis: <?php echo $ebook->penulis ?> </p>
                    <p>Penerbit: <?php echo $ebook->penerbit ?> </p>
                    <p>Tahun Terbit: <?php echo $ebook->tahun_terbit ?> </p>
                    <p>Harga: Rp <strike><b>Rp <?php echo number_format($ebook->harga_normal) ?></b></strike> | <b>Rp <?php echo number_format($ebook->harga_diskon) ?></b>
                        <font style="font-size:15px"><span class="badge badge-pill badge-primary"><?php echo $ebook->diskon ?>% OFF</span></font>
                    </p>
                    <p>Stok: <?php if ($ebook->stok == 'Tersedia') {
                                    echo "<font style='font-size:15px'><span class='badge badge-pill badge-success'>Tersedia</span></font>";
                                } else {
                                    echo "<font style='font-size:15px'><span class='badge badge-pill badge-primary'>Kosong</span></font>";
                                } ?></p>
                    <p>Jenis:
                        <a href="<?php echo base_url('jenis/read/') . $ebook->slug_jenis ?>">
                            <?php echo $ebook->nama_jenis ?>
                        </a> /
                        <a href="<?php echo base_url('jenis/read/') . $ebook->slug_jenis . "/" . $ebook->slug_kategori ?>">
                            <?php echo $ebook->nama_kategori ?>
                        </a>
                    </p>
                    <?php if ($ebook->link_tokped != null || $ebook->link_tokped != '') : ?>
                        <a href="<?= $ebook->link_tokped ?>">
                            <button class="btn btn btn-success"><i class="fa fa-cart-arrow-down"></i> Tokopedia</button>
                        </a>
                    <?php endif ?>
                    <?php if ($ebook->link_shopee != null || $ebook->link_shopee != '') : ?>
                        <a href="<?= $ebook->link_shopee ?>">
                            <button class="btn btn btn-danger"><i class="fa fa-cart-arrow-down"></i> Shopee</button>
                        </a>
                    <?php endif ?>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <h4>Deskripsi Ebook</h4>
                    <hr>
                    <p><?php echo $ebook->sinopsis ?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <h4>EBOOK LAINNYA</h4>
                    <hr>
                    <div class="row">
                        <?php foreach ($ebook_lainnya as $lainnya) { ?>
                            <div class="col-sm-3 col-lg-3 col-md-12 col-sm-3 col-xs-12">
                                <div class="card mb-3 box-shadow" style="min-width: 200px;">
                                    <a href="<?php echo base_url("ebook/$ebook->slug_ebook ") ?>" style="padding-left :40px; padding-right:40px;">
                                        <?php
                                        if (empty($lainnya->foto)) {
                                            echo "<img class='card-img-top' src='" . base_url() . "assets/images/no_image_thumb.png'>";
                                        } else {
                                            echo " <img class='card-img-top' src='" . base_url() . "assets/images/ebook/" . $lainnya->foto . '_thumb' . $lainnya->foto_type . "'> ";
                                        }
                                        ?>
                                    </a>
                                    <div class="card-body">
                                        <a href="<?php echo base_url("ebook/$lainnya->slug_ebook ") ?>">
                                            <p class="card-text"><b><?php echo character_limiter($lainnya->judul, 50) ?></b></p>
                                        </a>
                                        <br>
                                        <p align="center">
                                            <strike><b>Rp <?php echo number_format($lainnya->harga_normal) ?></b></strike><br>
                                            <b>Rp <?php echo number_format($lainnya->harga_diskon) ?></b>
                                            <font style="font-size:15px"><span class="badge badge-pill badge-primary"><?php echo $lainnya->diskon ?>% OFF</span></font>
                                        </p>
                                        <p align="center">
                                            <a href="<?php echo base_url('ebook/') . $lainnya->slug_ebook ?>">
                                                <button class="btn btn btn-danger"><i class="fa fa-eye"></i> Detail</button>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>

        <!-- Kolom kanan -->
        <?php $this->load->view('front/sidebar'); ?>

        <script src="<?php echo base_url('assets/plugins/zooming/build/zooming.min.js') ?>"></script>
        <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a979c495d0b9500130f346b&product=sticky-share-buttons"></script>
    </div>
    <?php $this->load->view('front/footer'); ?>