<?php $this->load->view('front/header'); ?>
<?php $this->load->view('front/navbar'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Register</li>
                </ol>
            </nav>
        </div>

        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>

        <div class="col-lg-12">
            <h1>Daftar Baru</h1>
            <hr>Sudah punya akun? Silahkan Login <a href="<?php echo base_url('login') ?>">disini</a>
            <hr>
            <div class="row">
                <div class="col-lg-12">
                    <?php echo form_open("auth/register"); ?>
                    <div class="box-body">
                        <?php echo $message; ?>
                        <div class="form-row">
                            <div class="form-group col-md-6"><label>Nama Lengkap</label>
                                <?php echo form_input($nama_lengkap); ?>
                            </div>
                            <div class="form-group col-md-6"><label>Email</label>
                                <?php echo form_input($email); ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6"><label>Password</label>
                                <?php echo form_password($password); ?>
                            </div>
                            <div class="form-group col-md-6"><label>Konfirmasi Password</label>
                                <?php echo form_password($password_confirm); ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6"><label>Nama Pena</label>
                                <?php echo form_input($nama_pena); ?>
                            </div>
                            <div class="form-group col-md-6"><label>No. Hp</label>
                                <?php echo form_input($nomor_hp); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="reset" class="btn btn-primary">Cancel</button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('front/footer'); ?>
</div>