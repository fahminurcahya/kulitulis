<!doctype html>
<html lang="en">

<head>
  <title><?php echo $title ?></title>
  <meta charset="UTF-8">
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.4 -->
  <link href="<?php echo base_url() ?>assets/template/backend/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="<?php echo base_url() ?>assets/template/backend/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
  <!-- Jquery -->
  <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-3.3.1.js"></script>
  <!-- Bootstrap 3.3.2 JS -->
  <script src="<?php echo base_url() ?>assets/template/backend/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Kalam:wght@700&family=Open+Sans:ital,wght@0,400;1,300&family=Roboto+Condensed&display=swap');
  </style>
  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/images/fav.jpg') ?>" />
</head>

<body style="background-color: #4498ee;" class="login-page">
  <div class="login-box">
    <div class="login-logo" style="font-family: 'Kalam', cursive;">
      <a href=""><b style="color: white;">Kulitulis</b></a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
      <h4 class="card-header">Reset Password</h4>
      <div class="card-body">
        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        <?php echo $message; ?>
        <?php echo form_open('auth/reset_password/' . $code); ?>
        <div class="form-group has-feedback"><label>Password Baru</label>
          <?php echo form_input($new_password); ?>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback"><label>Konfirmasi Password Baru</label>
          <?php echo form_input($new_password_confirm); ?>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <?php echo form_input($user_id); ?>
        <?php echo form_hidden($csrf); ?>
        <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
        <?php echo form_close(); ?>
      </div>
    </div><!-- /.login-box-body -->
  </div><!-- /.login-box -->

</body>

</html>