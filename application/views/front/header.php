<!doctype html>
<html lang="en">

<head>
    <title><?php echo $title; ?> | <?php echo $company_data->company_name; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php if ($this->uri->segment('1') == 'produk' and $this->uri->segment('2') != 'katalog' and $this->uri->segment('2') != 'cari_produk') { ?>
        <!-- Open Graph data -->
        <meta property="og:title" content="<?php echo $produk->judul_produk ?> | <?php echo $company_data->company_name ?>" />
        <meta property="og:type" content="product" />
        <meta property="og:url" content="<?php echo current_url() ?>" />
        <meta property="og:image" content="<?php echo base_url('assets/images/produk/') . $produk->foto . $produk->foto_type ?>" />
        <meta property="og:description" content="<?php echo character_limiter($produk->deskripsi, '50') ?>" />
    <?php } ?>
    <!-- core ui -->
    <link href="<?php echo base_url() ?>assets/template/frontend/css/theme/simplex.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/template/frontend/css/custom.css" rel="stylesheet">
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;1,400;1,500;1,600&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;1,400;1,500;1,600&display=swap');

        h4,
        h1,
        h2,
        h3,
        a {
            font-family: 'Playfair Display', serif;
            font-weight: bold;
        }

        p {
            font-family: 'Playfair Display', serif;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-horizon/0.1.1/bootstrap-horizon.css">
    <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-3.3.1.js" rel="stylesheet"></script>
    <script src="<?php echo base_url() ?>assets/template/frontend/js/bootstrap.min.js" rel="stylesheet"></script>
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- SLIDER -->
    <script src="<?php echo base_url('assets/plugins/slider/slippry-1.4.0/src/') ?>slippry.js"></script>
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/slider/slippry-1.4.0/dist/') ?>slippry.css" />
    <!-- dropdown menu plugin -->
    <link href="<?php echo base_url('assets/plugins/cssmenu/') ?>styles.css" rel="stylesheet">
    <script src="<?php echo base_url('assets/plugins/cssmenu/') ?>script.js"></script>
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/fav.jpg') ?>" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114929317-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-114929317-1');
    </script>
</head>

<body>