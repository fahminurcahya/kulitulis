		<footer>
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-2">
						<hr>
						<h5>Marketplace</h5>
						<hr>
						<li><a href="https://www.tokopedia.com/">Tokopedia</a></li>
						<li><a href="https://www.bukalapak.com/">Bukalapak</a></li>
						<li><a href="https://www.shopee.co.id/">Shopee</a></li>
					</div>
					<div class="col-lg-2">
						<hr>
						<h5>SocMed</h5>
						<hr>
						<li><a href="https://facebook.com">Facebook</a></li>
						<li><a href="https://twitter.com">Twitter</a></li>
						<li><a href="https://instagram.com">Instagram</a></li>
					</div>
					<div class="col-lg-3">
						<hr>
						<h5>Halaman</h5>
						<hr>
						<li><a href="<?php echo base_url() ?>">Home</a></li>
						<li><a href="">Event</a></li>
						<li><a href="">Katalog</a></li>
						<li><a href="">Profil</a></li>
					</div>
					<div class="col-lg-2">
						<hr>
						<h5>Kontak</h5>
						<hr>

						<?php echo $company_data->company_phone1 ?><br>
						<a href="https://api.whatsapp.com/send?phone=<?php echo $company_data->company_phone1 ?>>
							<button class=" btn btn-sm btn-success" type="submit" name="button">Chat via Whatsapp</button>
						</a><br><br>
						<?php echo $company_data->company_phone2 ?><br>
						<a href="https://api.whatsapp.com/send?phone=<?php echo $company_data->company_phone2 ?>>
							<button class=" btn btn-sm btn-success" type="submit" name="button">Chat via Whatsapp</button>
						</a>
					</div>
					<div class="col-md-3">
						<hr>
						<h5>Lokasi/ Map</h5>
						<hr>
						<?php echo htmlspecialchars_decode($company_data->company_maps) ?>
					</div>
					<div class="col-lg-12">
						<hr>
						<div class="row">
							<div class="col-md-9">
								<a href="#">Copyright &copy; 2022</a>
							</div>
							<div class="col-md-3"><a href="#top">Kembali ke atas</a></div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		</body>

		</html>