<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class JenisModel extends CI_Model
{
    public $table = 'tbl_jenis';
    public $id    = 'id_jenis';
    public $order = 'DESC';

    var $column = array('id_jenis', 'nama_jenis');

    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function delete($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->delete($this->table);
    }

    function ambil_jenis()
    {
        $sql_prov = $this->db->get('tbl_jenis');
        if ($sql_prov->num_rows() > 0) {
            foreach ($sql_prov->result_array() as $row) {
                $result[''] = '- Pilih Jenis -';
                $result[$row['id_jenis']] = ucwords(strtolower($row['nama_jenis']));
            }
            return $result;
        }
    }

    function get_list_by_jenis($slug, $limit = null, $offset = null)
    {
        $this->db->join('tbl_jenis', 'tbl_ebook.id_jenis=tbl_jenis.id_jenis');
        $this->db->where('tbl_jenis.slug_jenis', $slug);
        $this->db->limit($limit, $offset);

        return $this->db->get('tbl_ebook');
    }

    function get_by_jenis_nr($slug)
    {
        $this->db->join('tbl_jenis', 'tbl_ebook.id_jenis=tbl_jenis.id_jenis');
        $this->db->where('tbl_jenis.slug_jenis', $slug);

        return $this->db->get('tbl_ebook')->num_rows();
    }

    function get_list_by_kategori($slug, $limit = null, $offset = null)
    {
        $this->db->join('tbl_kategori', 'tbl_ebook.id_kategori=tbl_kategori.id_kategori');
        $this->db->where('tbl_kategori.slug_kategori', $slug);
        $this->db->limit($limit, $offset);

        return $this->db->get('tbl_ebook');
    }

    function get_by_kategori_nr($slug)
    {
        $this->db->join('tbl_kategori', 'tbl_ebook.id_kategori=tbl_kategori.id_kategori');
        $this->db->where('tbl_kategori.slug_kategori', $slug);

        return $this->db->get('tbl_ebook')->num_rows();
    }
}
