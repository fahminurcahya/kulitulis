<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class UsersModel extends CI_Model
{
    public $table = 'tbl_user';
    public $id    = 'id_user';
    public $order = 'ASC';

    // additional
    public function total_rows()
    {
        return $this->db->get('users')->num_rows();
    }

    // additional
    public function get_all_users()
    {
        $this->db->join('tbl_level', 'tbl_user.id_level = tbl_level.id_level');
        return $this->db->get('tbl_user');
    }

    // additional
    public function get_all_users_levels()
    {
        // get semua level kecuali superadmin
        $query = $this->db->select('id_level, nama_level')
            ->where('id_level != 1 OR nama_level != "superadmin"')
            ->order_by('nama_level', 'ASC')
            ->get('tbl_level');
        // get semua level kecuali superadmin

        if ($query->num_rows() > 0) {
            $data = array();
            foreach ($query->result_array() as $row) {
                $data[$row['id_level']] = $row['nama_level'];
            }
            return $data;
        }
    }

    // GET nama pena
    public function get_nama_pena($id_user)
    {
        // get nama pena
        $query = $this->db->select('nama_pena')
            ->where('id_user', $id_user)
            ->get('tbl_user');
        // get nama pena

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data = $row['nama_pena'];
            }
            return $data;
        }
    }
    // GET nama pena

    //Get user By Id
    public function get_by_id($id_user)
    {
        // get nama pena
        $this->db->where($this->id, $id_user);
        return $this->db->get($this->table)->row();
    }

    function count_all()
    {
        $this->db->where('id_level !=', 1);
        $this->db->where('id_level !=', 2);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
