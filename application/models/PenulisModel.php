<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class PenulisModel extends CI_Model
{
    public $table = 'tbl_tulisan';
    public $id    = 'id_tulisan';
    public $order = 'DESC';

    var $column = array('id_tulisan', 'judul', 'nama_pena', 'nama_jenis', 'nama_kategori', 'nama_lengkap', 'sinopsis', 'status', 'tgl_kirim', 'note');

    // draft data
    function draft($id, $data)
    {
        $id_tulisan = $data["id_tulisan"];
        $id_jenis = $data["id_jenis"];
        $id_kategori = $data["id_kategori"];
        $judul = $data["judul"];
        $sinopsis = $data["sinopsis"];
        $tulisan = $data["tulisan"];
        $tgl_kirim = $data["tgl_kirim"];
        $status = $data["status"];
        $flag_bayar = $data["flag_bayar"];
        $tgl_bayar = $data["tgl_bayar"];
        $created_at = $data["created_at"];
        $created_by = $data["created_by"];

        $query  = "INSERT INTO tbl_tulisan SET 
                        id_tulisan = '$id_tulisan',
                        id_jenis = '$id_jenis',
                        id_kategori = '$id_kategori',
                        id_user = '$id',
                        judul = '$judul',
                        sinopsis = '$sinopsis',
                        tulisan = '$tulisan',
                        tgl_kirim = '$tgl_kirim',
                        status = '$status',
                        flag_bayar = '$flag_bayar',
                        tgl_bayar = '$tgl_bayar',
                        created_at = '$created_at',
                        created_by = '$created_by'
                    ON DUPLICATE KEY UPDATE
                        id_jenis = '$id_jenis',
                        id_kategori = '$id_kategori',
                        judul = '$judul',
                        sinopsis = '$sinopsis',
                        tulisan = '$tulisan',
                        tgl_kirim = '$tgl_kirim',
                        status = '$status',
                        flag_bayar = '$flag_bayar',
                        tgl_bayar = '$tgl_bayar',
                        created_at = '$created_at',
                        created_by = '$created_by'
                    ";
        $sql    = $this->db->query($query);
    }

    function publish($id, $data)
    {
        $id_tulisan = $data["id_tulisan"];
        $id_jenis = $data["id_jenis"];
        $id_kategori = $data["id_kategori"];
        $judul = $data["judul"];
        $sinopsis = $data["sinopsis"];
        $tulisan = $data["tulisan"];
        $tgl_kirim = $data["tgl_kirim"];
        $status = $data["status"];
        $flag_bayar = $data["flag_bayar"];
        $tgl_bayar = $data["tgl_bayar"];
        $created_at = $data["created_at"];
        $created_by = $data["created_by"];

        $query  = "INSERT INTO tbl_tulisan SET 
                        id_tulisan = '$id_tulisan',
                        id_jenis = '$id_jenis',
                        id_kategori = '$id_kategori',
                        id_user = '$id',
                        judul = '$judul',
                        sinopsis = '$sinopsis',
                        tulisan = '$tulisan',
                        tgl_kirim = '$tgl_kirim',
                        status = '$status',
                        flag_bayar = '$flag_bayar',
                        tgl_bayar = '$tgl_bayar',
                        created_at = '$created_at',
                        created_by = '$created_by'
                    ON DUPLICATE KEY UPDATE
                        id_jenis = '$id_jenis',
                        id_kategori = '$id_kategori',
                        judul = '$judul',
                        sinopsis = '$sinopsis',
                        tulisan = '$tulisan',
                        tgl_kirim = '$tgl_kirim',
                        status = '$status',
                        flag_bayar = '$flag_bayar',
                        tgl_bayar = '$tgl_bayar',
                        created_at = '$created_at',
                        created_by = '$created_by'
                    ";
        $sql    = $this->db->query($query);
    }

    private function _get_datatables_query($status)
    {
        $id_user = $_SESSION['id_user'];
        $this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_tulisan.id_kategori');
        $this->db->join('tbl_jenis', 'tbl_jenis.id_jenis = tbl_tulisan.id_jenis');
        $this->db->join('tbl_user', 'tbl_user.id_user = tbl_tulisan.id_user');
        if ($status == 'S') {
            $this->db->where("(status='S' OR status='A' OR status='R')", NULL, FALSE);
        } else {
            $this->db->where("(status='R' OR status='D')", NULL, FALSE);
        }
        $this->db->where('tbl_tulisan.id_user', $id_user);
        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($status)
    {
        $this->_get_datatables_query($status);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($status)
    {
        $this->_get_datatables_query($status);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_by_id($id)
    {
        $this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_tulisan.id_kategori');
        $this->db->join('tbl_jenis', 'tbl_jenis.id_jenis = tbl_tulisan.id_jenis');
        $this->db->join('tbl_user', 'tbl_user.id_user = tbl_tulisan.id_user');
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function get_data_by_id_user($status, $flag)
    {
        $id_user = $_SESSION['id_user'];
        $this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_tulisan.id_kategori');
        $this->db->join('tbl_jenis', 'tbl_jenis.id_jenis = tbl_tulisan.id_jenis');
        $this->db->join('tbl_user', 'tbl_user.id_user = tbl_tulisan.id_user');
        $this->db->where('tbl_tulisan.status', $status);
        $this->db->where('tbl_tulisan.flag_bayar', $flag);
        $this->db->where('tbl_tulisan.id_user', $id_user);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->num_rows();
    }


    function get_datatables_limit($status)
    {
        $id_user = $_SESSION['id_user'];
        $this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_tulisan.id_kategori');
        $this->db->join('tbl_jenis', 'tbl_jenis.id_jenis = tbl_tulisan.id_jenis');
        $this->db->join('tbl_user', 'tbl_user.id_user = tbl_tulisan.id_user');
        if ($status == 'S') {
            $this->db->where("(status='S')", NULL, FALSE);
        } else {
            $this->db->where("(status='D')", NULL, FALSE);
        }
        $this->db->where('tbl_tulisan.id_user', $id_user);
        $this->db->limit(10, 0);
        $this->db->order_by("tbl_tulisan.created_at", "desc");
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }
}
