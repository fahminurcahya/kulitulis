<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class EbookModel extends CI_Model
{
    public $table = 'tbl_ebook';
    public $id    = 'id_ebook';
    public $order = 'DESC';

    var $column = array('id_ebook', 'judul');

    private function _get_datatables_query()
    {
        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function del_by_id($id)
    {
        $this->db->select("foto, foto_type");
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_all_new_home()
    {
        $this->db->limit(5);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }


    function get_by_id_front($id)
    {
        $this->db->join('tbl_jenis', 'tbl_ebook.id_jenis = tbl_jenis.id_jenis', 'left');
        $this->db->join('tbl_kategori', 'tbl_ebook.id_kategori = tbl_kategori.id_kategori', 'left');
        $this->db->where('slug_ebook', $id);
        return $this->db->get($this->table)->row();
    }

    function get_random()
    {
        $this->db->limit(6);
        $this->db->order_by('judul', 'RANDOM');
        return $this->db->get($this->table)->result();
    }

    // get all
    function get_cari_ebook()
    {
        $cari = $this->input->post('cari');

        $this->db->like('judul', $cari);
        return $this->db->get($this->table)->result();
    }

    // get total rows
    function total_rows()
    {
        return $this->db->get($this->table)->num_rows();
    }

    function get_all_katalog($per_page, $dari)
    {
        $this->db->order_by($this->id, 'DESC');
        $query = $this->db->get($this->table, $per_page, $dari);
        return $query->result();
    }

    public function count_best_seller()
    {
        $this->db->where('best_seller', '1');
        return $this->db->get($this->table)->num_rows();
    }


    public function get_datatables_best()
    {
        $this->db->where('best_seller', '1');
        return $this->db->get($this->table)->result();
    }

    function get_all_best_home()
    {
        $this->db->limit(5);
        $this->db->where('best_seller', '1');
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
}
