<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TulisanModel extends CI_Model
{
    public $table = 'tbl_tulisan';
    public $id    = 'id_tulisan';
    public $order = 'DESC';

    var $column = array('id_tulisan', 'judul', 'id_user', 'nama_jenis', 'nama_kategori', 'nama_lengkap', 'nama_pena', 'tgl_kirim');

    private function _get_datatables_query($status, $flag)
    {
        $this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_tulisan.id_kategori');
        $this->db->join('tbl_jenis', 'tbl_jenis.id_jenis = tbl_tulisan.id_jenis');
        $this->db->join('tbl_user', 'tbl_user.id_user = tbl_tulisan.id_user');
        $this->db->where('status', $status);
        $this->db->where('flag_bayar', $flag);
        if ($status == 'S') {
            $this->db->order_by('tgl_kirim', 'DESC');
        } else if ($status == 'A' && $flag == 'N') {
            $this->db->order_by('tgl_disetujui', 'DESC');
        } else if ($status == 'A' && $flag == 'Y') {
            $this->db->order_by('tgl_bayar', 'DESC');
        }

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($status, $flag)
    {
        $this->_get_datatables_query($status, $flag);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($status, $flag)
    {
        $this->_get_datatables_query($status, $flag);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_by_id($id)
    {
        $this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_tulisan.id_kategori');
        $this->db->join('tbl_jenis', 'tbl_jenis.id_jenis = tbl_tulisan.id_jenis');
        $this->db->join('tbl_user', 'tbl_user.id_user = tbl_tulisan.id_user');
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function count_data($status, $flag)
    {
        $this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_tulisan.id_kategori');
        $this->db->join('tbl_jenis', 'tbl_jenis.id_jenis = tbl_tulisan.id_jenis');
        $this->db->join('tbl_user', 'tbl_user.id_user = tbl_tulisan.id_user');
        $this->db->where('tbl_tulisan.status', $status);
        $this->db->where('tbl_tulisan.flag_bayar', $flag);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_datatables_limit($status)
    {
        $this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_tulisan.id_kategori');
        $this->db->join('tbl_jenis', 'tbl_jenis.id_jenis = tbl_tulisan.id_jenis');
        $this->db->join('tbl_user', 'tbl_user.id_user = tbl_tulisan.id_user');
        if ($status == 'S') {
            $this->db->where("status", 'S');
            $this->db->order_by('tgl_kirim', 'DESC');
        } else {
            $this->db->where("status", 'D');
            $this->db->order_by('created_at', 'DESC');
        }
        // $this->db->limit(10, 0);
        // $this->db->order_by("tbl_tulisan.tgl_kirim", "desc");
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }
}
